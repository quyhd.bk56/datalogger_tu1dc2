﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataLogger.Entities;
using System.Resources;
using System.Globalization;
using DataLogger.Utils;
using DataLogger.Data;
using System.Reflection;

namespace DataLogger
{
    public partial class frm1HourMPS : Form
    {

        LanguageService lang;

        public data_value obj_data_value { get; set; }
        public frm1HourMPS()
        {
            InitializeComponent();
        }

        public frm1HourMPS(data_value obj, LanguageService _lang)
        {
            InitializeComponent();
            obj_data_value = obj;
            //res_man = obj_res_man;
            //cul = obj_cul;
            lang = _lang;
            switch_language();
        }
        private void switch_language()
        {
            this.lblHeaderTitle.Text = lang.getText("form_1_hour_title");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm1HourMPS_Load(object sender, EventArgs e)
        {
            GlobalVar.stationSettings = new station_repository().get_info();
            GlobalVar.moduleSettings = new module_repository().get_all();
            for (int i = 1; i <= GlobalVar.moduleSettings.Count(); i++)
            {
                foreach (var item in GlobalVar.moduleSettings)
                {
                    string currentvar = "var" + i.ToString();
                    string currentlabelname = "txt" + currentvar;
                    string currentlabelunit = "txt" + currentvar + "Unit";
                    string currentlabelvalue = "txt" + currentvar + "Value";
                    if (item.item_name.Equals(currentvar))
                    {

                        frmNewMain.ClearLabel(this, item.display_name, currentlabelname);
                        frmNewMain.ClearLabel(this, item.unit, currentlabelunit);

                        string param = currentvar;  //VD : var1
                        Type paramType = typeof(data_value);
                        PropertyInfo prop = paramType.GetProperty(param);
                        double value = (double)prop.GetValue(obj_data_value); //VD : lay display_var = data.var1
                        frmNewMain.ClearTextbox(this, value.ToString("##0.00"), currentlabelvalue);
                    }
                }
            }
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void txtvar1_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar1.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar1.Text,
                 new Font(txtvar1.Font.FontFamily, txtvar1.Font.Size, txtvar1.Font.Style)).Width)
            {
                txtvar1.Font = new Font(txtvar1.Font.FontFamily, txtvar1.Font.Size - 0.5f, txtvar1.Font.Style);
            }
        }

        private void txtvar2_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar2.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar2.Text,
                 new Font(txtvar2.Font.FontFamily, txtvar2.Font.Size, txtvar2.Font.Style)).Width)
            {
                txtvar2.Font = new Font(txtvar2.Font.FontFamily, txtvar2.Font.Size - 0.5f, txtvar2.Font.Style);
            }
        }

        private void txtvar3_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar3.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar3.Text,
                 new Font(txtvar3.Font.FontFamily, txtvar3.Font.Size, txtvar3.Font.Style)).Width)
            {
                txtvar3.Font = new Font(txtvar3.Font.FontFamily, txtvar3.Font.Size - 0.5f, txtvar3.Font.Style);
            }
        }

        private void txtvar4_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar4.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar4.Text,
                 new Font(txtvar4.Font.FontFamily, txtvar4.Font.Size, txtvar4.Font.Style)).Width)
            {
                txtvar4.Font = new Font(txtvar4.Font.FontFamily, txtvar4.Font.Size - 0.5f, txtvar4.Font.Style);
            }
        }

        private void txtvar5_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar5.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar5.Text,
                 new Font(txtvar5.Font.FontFamily, txtvar5.Font.Size, txtvar5.Font.Style)).Width)
            {
                txtvar5.Font = new Font(txtvar5.Font.FontFamily, txtvar5.Font.Size - 0.5f, txtvar5.Font.Style);
            }
        }

        private void txtvar6_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar6.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar6.Text,
                 new Font(txtvar6.Font.FontFamily, txtvar6.Font.Size, txtvar6.Font.Style)).Width)
            {
                txtvar6.Font = new Font(txtvar6.Font.FontFamily, txtvar6.Font.Size - 0.5f, txtvar6.Font.Style);
            }
        }

        private void txtvar7_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar7.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar7.Text,
                    new Font(txtvar7.Font.FontFamily, txtvar7.Font.Size, txtvar7.Font.Style)).Width)
            {
                txtvar7.Font = new Font(txtvar7.Font.FontFamily, txtvar7.Font.Size - 0.5f, txtvar7.Font.Style);
            }
        }

        private void txtvar8_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar8.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar8.Text,
        new Font(txtvar8.Font.FontFamily, txtvar8.Font.Size, txtvar8.Font.Style)).Width)
            {
                txtvar8.Font = new Font(txtvar8.Font.FontFamily, txtvar8.Font.Size - 0.5f, txtvar8.Font.Style);
            }
        }

        private void txtvar9_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar9.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar9.Text,
        new Font(txtvar9.Font.FontFamily, txtvar9.Font.Size, txtvar9.Font.Style)).Width)
            {
                txtvar9.Font = new Font(txtvar9.Font.FontFamily, txtvar9.Font.Size - 0.5f, txtvar9.Font.Style);
            }
        }

        private void txtvar10_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar10.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar10.Text,
        new Font(txtvar10.Font.FontFamily, txtvar10.Font.Size, txtvar10.Font.Style)).Width)
            {
                txtvar10.Font = new Font(txtvar10.Font.FontFamily, txtvar10.Font.Size - 0.5f, txtvar10.Font.Style);
            }
        }

        private void txtvar11_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar11.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar11.Text,
        new Font(txtvar11.Font.FontFamily, txtvar11.Font.Size, txtvar11.Font.Style)).Width)
            {
                txtvar11.Font = new Font(txtvar11.Font.FontFamily, txtvar11.Font.Size - 0.5f, txtvar11.Font.Style);
            }
        }

        private void txtvar12_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar12.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar12.Text,
new Font(txtvar12.Font.FontFamily, txtvar12.Font.Size, txtvar12.Font.Style)).Width)
            {
                txtvar12.Font = new Font(txtvar12.Font.FontFamily, txtvar12.Font.Size - 0.5f, txtvar12.Font.Style);
            }
        }

        private void txtvar13_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar13.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar13.Text,
new Font(txtvar13.Font.FontFamily, txtvar13.Font.Size, txtvar13.Font.Style)).Width)
            {
                txtvar13.Font = new Font(txtvar13.Font.FontFamily, txtvar13.Font.Size - 0.5f, txtvar13.Font.Style);
            }
        }

        private void txtvar1Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar1Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar1Unit.Text,
        new Font(txtvar1Unit.Font.FontFamily, txtvar1Unit.Font.Size, txtvar1Unit.Font.Style)).Width)
            {
                txtvar1Unit.Font = new Font(txtvar1Unit.Font.FontFamily, txtvar1Unit.Font.Size - 0.5f, txtvar1Unit.Font.Style);
            }
        }

        private void txtvar2Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar2Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar2Unit.Text,
        new Font(txtvar2Unit.Font.FontFamily, txtvar2Unit.Font.Size, txtvar2Unit.Font.Style)).Width)
            {
                txtvar2Unit.Font = new Font(txtvar2Unit.Font.FontFamily, txtvar2Unit.Font.Size - 0.5f, txtvar2Unit.Font.Style);
            }
        }

        private void txtvar3Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar3Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar3Unit.Text,
        new Font(txtvar3Unit.Font.FontFamily, txtvar3Unit.Font.Size, txtvar3Unit.Font.Style)).Width)
            {
                txtvar3Unit.Font = new Font(txtvar3Unit.Font.FontFamily, txtvar3Unit.Font.Size - 0.5f, txtvar3Unit.Font.Style);
            }
        }

        private void txtvar4Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar4Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar4Unit.Text,
        new Font(txtvar4Unit.Font.FontFamily, txtvar4Unit.Font.Size, txtvar4Unit.Font.Style)).Width)
            {
                txtvar4Unit.Font = new Font(txtvar4Unit.Font.FontFamily, txtvar4Unit.Font.Size - 0.5f, txtvar4Unit.Font.Style);
            }
        }

        private void txtvar5Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar5Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar5Unit.Text,
        new Font(txtvar5Unit.Font.FontFamily, txtvar5Unit.Font.Size, txtvar5Unit.Font.Style)).Width)
            {
                txtvar5Unit.Font = new Font(txtvar5Unit.Font.FontFamily, txtvar5Unit.Font.Size - 0.5f, txtvar5Unit.Font.Style);
            }
        }

        private void txtvar6Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar6Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar6Unit.Text,
        new Font(txtvar6Unit.Font.FontFamily, txtvar6Unit.Font.Size, txtvar6Unit.Font.Style)).Width)
            {
                txtvar6Unit.Font = new Font(txtvar6Unit.Font.FontFamily, txtvar6Unit.Font.Size - 0.5f, txtvar6Unit.Font.Style);
            }
        }

        private void txtvar7Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar7Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar7Unit.Text,
        new Font(txtvar7Unit.Font.FontFamily, txtvar7Unit.Font.Size, txtvar7Unit.Font.Style)).Width)
            {
                txtvar7Unit.Font = new Font(txtvar7Unit.Font.FontFamily, txtvar7Unit.Font.Size - 0.5f, txtvar7Unit.Font.Style);
            }
        }

        private void txtvar8Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar8Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar8Unit.Text,
        new Font(txtvar8Unit.Font.FontFamily, txtvar8Unit.Font.Size, txtvar8Unit.Font.Style)).Width)
            {
                txtvar8Unit.Font = new Font(txtvar8Unit.Font.FontFamily, txtvar8Unit.Font.Size - 0.5f, txtvar8Unit.Font.Style);
            }
        }

        private void txtvar9Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar9Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar9Unit.Text,
        new Font(txtvar9Unit.Font.FontFamily, txtvar9Unit.Font.Size, txtvar9Unit.Font.Style)).Width)
            {
                txtvar9Unit.Font = new Font(txtvar9Unit.Font.FontFamily, txtvar9Unit.Font.Size - 0.5f, txtvar9Unit.Font.Style);
            }
        }

        private void txtvar10Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar10Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar10Unit.Text,
        new Font(txtvar10Unit.Font.FontFamily, txtvar10Unit.Font.Size, txtvar10Unit.Font.Style)).Width)
            {
                txtvar10Unit.Font = new Font(txtvar10Unit.Font.FontFamily, txtvar10Unit.Font.Size - 0.5f, txtvar10Unit.Font.Style);
            }
        }

        private void txtvar11Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar11Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar11Unit.Text,
        new Font(txtvar11Unit.Font.FontFamily, txtvar11Unit.Font.Size, txtvar11Unit.Font.Style)).Width)
            {
                txtvar11Unit.Font = new Font(txtvar11Unit.Font.FontFamily, txtvar11Unit.Font.Size - 0.5f, txtvar11Unit.Font.Style);
            }
        }

        private void txtvar12Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar12Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar12Unit.Text,
new Font(txtvar12Unit.Font.FontFamily, txtvar12Unit.Font.Size, txtvar12Unit.Font.Style)).Width)
            {
                txtvar12Unit.Font = new Font(txtvar12Unit.Font.FontFamily, txtvar12Unit.Font.Size - 0.5f, txtvar12Unit.Font.Style);
            }
        }

        private void txtvar13Unit_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar13Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar13Unit.Text,
new Font(txtvar13Unit.Font.FontFamily, txtvar13Unit.Font.Size, txtvar13Unit.Font.Style)).Width)
            {
                txtvar13Unit.Font = new Font(txtvar13Unit.Font.FontFamily, txtvar13Unit.Font.Size - 0.5f, txtvar13Unit.Font.Style);
            }
        }
    }
}
