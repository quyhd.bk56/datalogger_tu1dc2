﻿namespace DataLogger
{
    partial class frmConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.cbModule = new System.Windows.Forms.ComboBox();
            this.lblModuleComport = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.var18ErrorMax = new System.Windows.Forms.TextBox();
            this.var17ErrorMax = new System.Windows.Forms.TextBox();
            this.var16ErrorMax = new System.Windows.Forms.TextBox();
            this.var15ErrorMax = new System.Windows.Forms.TextBox();
            this.var14ErrorMax = new System.Windows.Forms.TextBox();
            this.var13ErrorMax = new System.Windows.Forms.TextBox();
            this.var12ErrorMax = new System.Windows.Forms.TextBox();
            this.var11ErrorMax = new System.Windows.Forms.TextBox();
            this.var10ErrorMax = new System.Windows.Forms.TextBox();
            this.var9ErrorMax = new System.Windows.Forms.TextBox();
            this.var8ErrorMax = new System.Windows.Forms.TextBox();
            this.var7ErrorMax = new System.Windows.Forms.TextBox();
            this.var6ErrorMax = new System.Windows.Forms.TextBox();
            this.var5ErrorMax = new System.Windows.Forms.TextBox();
            this.var4ErrorMax = new System.Windows.Forms.TextBox();
            this.var3ErrorMax = new System.Windows.Forms.TextBox();
            this.var2ErrorMax = new System.Windows.Forms.TextBox();
            this.var1ErrorMax = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.var18DisplayName = new System.Windows.Forms.TextBox();
            this.var17DisplayName = new System.Windows.Forms.TextBox();
            this.var16DisplayName = new System.Windows.Forms.TextBox();
            this.var15DisplayName = new System.Windows.Forms.TextBox();
            this.var14DisplayName = new System.Windows.Forms.TextBox();
            this.var13DisplayName = new System.Windows.Forms.TextBox();
            this.var18Type = new System.Windows.Forms.TextBox();
            this.var17Type = new System.Windows.Forms.TextBox();
            this.var16Type = new System.Windows.Forms.TextBox();
            this.var15Type = new System.Windows.Forms.TextBox();
            this.var14Type = new System.Windows.Forms.TextBox();
            this.var13Type = new System.Windows.Forms.TextBox();
            this.var18StatusColumn = new System.Windows.Forms.TextBox();
            this.var17StatusColumn = new System.Windows.Forms.TextBox();
            this.var16StatusColumn = new System.Windows.Forms.TextBox();
            this.var15StatusColumn = new System.Windows.Forms.TextBox();
            this.var14StatusColumn = new System.Windows.Forms.TextBox();
            this.var13StatusColumn = new System.Windows.Forms.TextBox();
            this.var18ValueColumn = new System.Windows.Forms.TextBox();
            this.var18Unit = new System.Windows.Forms.TextBox();
            this.var17ValueColumn = new System.Windows.Forms.TextBox();
            this.var17Unit = new System.Windows.Forms.TextBox();
            this.var16ValueColumn = new System.Windows.Forms.TextBox();
            this.var16Unit = new System.Windows.Forms.TextBox();
            this.var15ValueColumn = new System.Windows.Forms.TextBox();
            this.var15Unit = new System.Windows.Forms.TextBox();
            this.var14ValueColumn = new System.Windows.Forms.TextBox();
            this.var14Unit = new System.Windows.Forms.TextBox();
            this.var13ValueColumn = new System.Windows.Forms.TextBox();
            this.var13Unit = new System.Windows.Forms.TextBox();
            this.var18Offset = new System.Windows.Forms.TextBox();
            this.var17Offset = new System.Windows.Forms.TextBox();
            this.var16Offset = new System.Windows.Forms.TextBox();
            this.var15Offset = new System.Windows.Forms.TextBox();
            this.var14Offset = new System.Windows.Forms.TextBox();
            this.var13Offset = new System.Windows.Forms.TextBox();
            this.var18OutputMax = new System.Windows.Forms.TextBox();
            this.var18OutputMin = new System.Windows.Forms.TextBox();
            this.var18InputMax = new System.Windows.Forms.TextBox();
            this.var18InputMin = new System.Windows.Forms.TextBox();
            this.var18Channel = new System.Windows.Forms.TextBox();
            this.var17OutputMax = new System.Windows.Forms.TextBox();
            this.var17OutputMin = new System.Windows.Forms.TextBox();
            this.var17InputMax = new System.Windows.Forms.TextBox();
            this.var17InputMin = new System.Windows.Forms.TextBox();
            this.var17Channel = new System.Windows.Forms.TextBox();
            this.var16OutputMax = new System.Windows.Forms.TextBox();
            this.var16OutputMin = new System.Windows.Forms.TextBox();
            this.var16InputMax = new System.Windows.Forms.TextBox();
            this.var16InputMin = new System.Windows.Forms.TextBox();
            this.var16Channel = new System.Windows.Forms.TextBox();
            this.var15OutputMax = new System.Windows.Forms.TextBox();
            this.var15OutputMin = new System.Windows.Forms.TextBox();
            this.var15InputMax = new System.Windows.Forms.TextBox();
            this.var15InputMin = new System.Windows.Forms.TextBox();
            this.var15Channel = new System.Windows.Forms.TextBox();
            this.var14OutputMax = new System.Windows.Forms.TextBox();
            this.var14OutputMin = new System.Windows.Forms.TextBox();
            this.var14InputMax = new System.Windows.Forms.TextBox();
            this.var14InputMin = new System.Windows.Forms.TextBox();
            this.var14Channel = new System.Windows.Forms.TextBox();
            this.var13OutputMax = new System.Windows.Forms.TextBox();
            this.var13OutputMin = new System.Windows.Forms.TextBox();
            this.var13InputMax = new System.Windows.Forms.TextBox();
            this.var13InputMin = new System.Windows.Forms.TextBox();
            this.var13Channel = new System.Windows.Forms.TextBox();
            this.var12DisplayName = new System.Windows.Forms.TextBox();
            this.var11DisplayName = new System.Windows.Forms.TextBox();
            this.var10DisplayName = new System.Windows.Forms.TextBox();
            this.var9DisplayName = new System.Windows.Forms.TextBox();
            this.var8DisplayName = new System.Windows.Forms.TextBox();
            this.var7DisplayName = new System.Windows.Forms.TextBox();
            this.var12Type = new System.Windows.Forms.TextBox();
            this.var11Type = new System.Windows.Forms.TextBox();
            this.var10Type = new System.Windows.Forms.TextBox();
            this.var9Type = new System.Windows.Forms.TextBox();
            this.var8Type = new System.Windows.Forms.TextBox();
            this.var7Type = new System.Windows.Forms.TextBox();
            this.var12StatusColumn = new System.Windows.Forms.TextBox();
            this.var11StatusColumn = new System.Windows.Forms.TextBox();
            this.var10StatusColumn = new System.Windows.Forms.TextBox();
            this.var9StatusColumn = new System.Windows.Forms.TextBox();
            this.var8StatusColumn = new System.Windows.Forms.TextBox();
            this.var7StatusColumn = new System.Windows.Forms.TextBox();
            this.var12ValueColumn = new System.Windows.Forms.TextBox();
            this.var12Unit = new System.Windows.Forms.TextBox();
            this.var11ValueColumn = new System.Windows.Forms.TextBox();
            this.var11Unit = new System.Windows.Forms.TextBox();
            this.var10ValueColumn = new System.Windows.Forms.TextBox();
            this.var10Unit = new System.Windows.Forms.TextBox();
            this.var9ValueColumn = new System.Windows.Forms.TextBox();
            this.var9Unit = new System.Windows.Forms.TextBox();
            this.var8ValueColumn = new System.Windows.Forms.TextBox();
            this.var8Unit = new System.Windows.Forms.TextBox();
            this.var7ValueColumn = new System.Windows.Forms.TextBox();
            this.var7Unit = new System.Windows.Forms.TextBox();
            this.var12Offset = new System.Windows.Forms.TextBox();
            this.var11Offset = new System.Windows.Forms.TextBox();
            this.var10Offset = new System.Windows.Forms.TextBox();
            this.var9Offset = new System.Windows.Forms.TextBox();
            this.var8Offset = new System.Windows.Forms.TextBox();
            this.var7Offset = new System.Windows.Forms.TextBox();
            this.var12OutputMax = new System.Windows.Forms.TextBox();
            this.var12OutputMin = new System.Windows.Forms.TextBox();
            this.var12InputMax = new System.Windows.Forms.TextBox();
            this.var12InputMin = new System.Windows.Forms.TextBox();
            this.var12Channel = new System.Windows.Forms.TextBox();
            this.var11OutputMax = new System.Windows.Forms.TextBox();
            this.var11OutputMin = new System.Windows.Forms.TextBox();
            this.var11InputMax = new System.Windows.Forms.TextBox();
            this.var11InputMin = new System.Windows.Forms.TextBox();
            this.var11Channel = new System.Windows.Forms.TextBox();
            this.var10OutputMax = new System.Windows.Forms.TextBox();
            this.var10OutputMin = new System.Windows.Forms.TextBox();
            this.var10InputMax = new System.Windows.Forms.TextBox();
            this.var10InputMin = new System.Windows.Forms.TextBox();
            this.var10Channel = new System.Windows.Forms.TextBox();
            this.var9OutputMax = new System.Windows.Forms.TextBox();
            this.var9OutputMin = new System.Windows.Forms.TextBox();
            this.var9InputMax = new System.Windows.Forms.TextBox();
            this.var9InputMin = new System.Windows.Forms.TextBox();
            this.var9Channel = new System.Windows.Forms.TextBox();
            this.var8OutputMax = new System.Windows.Forms.TextBox();
            this.var8OutputMin = new System.Windows.Forms.TextBox();
            this.var8InputMax = new System.Windows.Forms.TextBox();
            this.var8InputMin = new System.Windows.Forms.TextBox();
            this.var8Channel = new System.Windows.Forms.TextBox();
            this.var7OutputMax = new System.Windows.Forms.TextBox();
            this.var7OutputMin = new System.Windows.Forms.TextBox();
            this.var7InputMax = new System.Windows.Forms.TextBox();
            this.var7InputMin = new System.Windows.Forms.TextBox();
            this.var7Channel = new System.Windows.Forms.TextBox();
            this.var6DisplayName = new System.Windows.Forms.TextBox();
            this.var5DisplayName = new System.Windows.Forms.TextBox();
            this.var4DisplayName = new System.Windows.Forms.TextBox();
            this.var3DisplayName = new System.Windows.Forms.TextBox();
            this.var2DisplayName = new System.Windows.Forms.TextBox();
            this.var1DisplayName = new System.Windows.Forms.TextBox();
            this.var6Type = new System.Windows.Forms.TextBox();
            this.var5Type = new System.Windows.Forms.TextBox();
            this.var4Type = new System.Windows.Forms.TextBox();
            this.var3Type = new System.Windows.Forms.TextBox();
            this.var2Type = new System.Windows.Forms.TextBox();
            this.var1Type = new System.Windows.Forms.TextBox();
            this.var6StatusColumn = new System.Windows.Forms.TextBox();
            this.var5StatusColumn = new System.Windows.Forms.TextBox();
            this.var4StatusColumn = new System.Windows.Forms.TextBox();
            this.var3StatusColumn = new System.Windows.Forms.TextBox();
            this.var2StatusColumn = new System.Windows.Forms.TextBox();
            this.var1StatusColumn = new System.Windows.Forms.TextBox();
            this.var6ValueColumn = new System.Windows.Forms.TextBox();
            this.var6Unit = new System.Windows.Forms.TextBox();
            this.var5ValueColumn = new System.Windows.Forms.TextBox();
            this.var5Unit = new System.Windows.Forms.TextBox();
            this.var4ValueColumn = new System.Windows.Forms.TextBox();
            this.var4Unit = new System.Windows.Forms.TextBox();
            this.var3ValueColumn = new System.Windows.Forms.TextBox();
            this.var3Unit = new System.Windows.Forms.TextBox();
            this.var2ValueColumn = new System.Windows.Forms.TextBox();
            this.var2Unit = new System.Windows.Forms.TextBox();
            this.var1ValueColumn = new System.Windows.Forms.TextBox();
            this.var1Unit = new System.Windows.Forms.TextBox();
            this.var6Offset = new System.Windows.Forms.TextBox();
            this.var5Offset = new System.Windows.Forms.TextBox();
            this.var4Offset = new System.Windows.Forms.TextBox();
            this.var3Offset = new System.Windows.Forms.TextBox();
            this.var2Offset = new System.Windows.Forms.TextBox();
            this.var1Offset = new System.Windows.Forms.TextBox();
            this.var6OutputMax = new System.Windows.Forms.TextBox();
            this.var6OutputMin = new System.Windows.Forms.TextBox();
            this.var6InputMax = new System.Windows.Forms.TextBox();
            this.var6InputMin = new System.Windows.Forms.TextBox();
            this.var6Channel = new System.Windows.Forms.TextBox();
            this.var5OutputMax = new System.Windows.Forms.TextBox();
            this.var5OutputMin = new System.Windows.Forms.TextBox();
            this.var5InputMax = new System.Windows.Forms.TextBox();
            this.var5InputMin = new System.Windows.Forms.TextBox();
            this.var5Channel = new System.Windows.Forms.TextBox();
            this.var4OutputMax = new System.Windows.Forms.TextBox();
            this.var4OutputMin = new System.Windows.Forms.TextBox();
            this.var4InputMax = new System.Windows.Forms.TextBox();
            this.var4InputMin = new System.Windows.Forms.TextBox();
            this.var4Channel = new System.Windows.Forms.TextBox();
            this.var3OutputMax = new System.Windows.Forms.TextBox();
            this.var3OutputMin = new System.Windows.Forms.TextBox();
            this.var3InputMax = new System.Windows.Forms.TextBox();
            this.var3InputMin = new System.Windows.Forms.TextBox();
            this.var3Channel = new System.Windows.Forms.TextBox();
            this.var2OutputMax = new System.Windows.Forms.TextBox();
            this.var2OutputMin = new System.Windows.Forms.TextBox();
            this.var2InputMax = new System.Windows.Forms.TextBox();
            this.var2InputMin = new System.Windows.Forms.TextBox();
            this.var2Channel = new System.Windows.Forms.TextBox();
            this.var1OutputMax = new System.Windows.Forms.TextBox();
            this.var1OutputMin = new System.Windows.Forms.TextBox();
            this.var1InputMax = new System.Windows.Forms.TextBox();
            this.var1InputMin = new System.Windows.Forms.TextBox();
            this.var1Channel = new System.Windows.Forms.TextBox();
            this.var18Module = new System.Windows.Forms.ComboBox();
            this.var17Module = new System.Windows.Forms.ComboBox();
            this.var18 = new System.Windows.Forms.Label();
            this.var17 = new System.Windows.Forms.Label();
            this.var16Module = new System.Windows.Forms.ComboBox();
            this.var15Module = new System.Windows.Forms.ComboBox();
            this.var16 = new System.Windows.Forms.Label();
            this.var15 = new System.Windows.Forms.Label();
            this.var14Module = new System.Windows.Forms.ComboBox();
            this.var13Module = new System.Windows.Forms.ComboBox();
            this.var14 = new System.Windows.Forms.Label();
            this.var13 = new System.Windows.Forms.Label();
            this.var12Module = new System.Windows.Forms.ComboBox();
            this.var11Module = new System.Windows.Forms.ComboBox();
            this.var12 = new System.Windows.Forms.Label();
            this.var11 = new System.Windows.Forms.Label();
            this.var10Module = new System.Windows.Forms.ComboBox();
            this.var9Module = new System.Windows.Forms.ComboBox();
            this.var10 = new System.Windows.Forms.Label();
            this.var9 = new System.Windows.Forms.Label();
            this.var8Module = new System.Windows.Forms.ComboBox();
            this.var7Module = new System.Windows.Forms.ComboBox();
            this.var8 = new System.Windows.Forms.Label();
            this.var7 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.Unit = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.var6Module = new System.Windows.Forms.ComboBox();
            this.var5Module = new System.Windows.Forms.ComboBox();
            this.var6 = new System.Windows.Forms.Label();
            this.var5 = new System.Windows.Forms.Label();
            this.var4Module = new System.Windows.Forms.ComboBox();
            this.var3Module = new System.Windows.Forms.ComboBox();
            this.var4 = new System.Windows.Forms.Label();
            this.var3 = new System.Windows.Forms.Label();
            this.var2Module = new System.Windows.Forms.ComboBox();
            this.var1Module = new System.Windows.Forms.ComboBox();
            this.var2 = new System.Windows.Forms.Label();
            this.var1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnSOCKET = new System.Windows.Forms.Button();
            this.txtSocketPort = new System.Windows.Forms.TextBox();
            this.txtStationID = new System.Windows.Forms.TextBox();
            this.txtStationName = new System.Windows.Forms.TextBox();
            this.lblSocketPort = new System.Windows.Forms.Label();
            this.lblStationID = new System.Windows.Forms.Label();
            this.lblStationName = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textPwd = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.dtpLastedValue = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbFlag = new System.Windows.Forms.TextBox();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.BackgroundImage = global::DataLogger.Properties.Resources.Save_Button_1;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.Window;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(759, 386);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(115, 49);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.Transparent;
            this.btnRefresh.BackgroundImage = global::DataLogger.Properties.Resources.Refesh_button;
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRefresh.FlatAppearance.BorderSize = 0;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.Window;
            this.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRefresh.Location = new System.Drawing.Point(636, 386);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(117, 49);
            this.btnRefresh.TabIndex = 12;
            this.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.cbModule);
            this.tabPage4.Controls.Add(this.lblModuleComport);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage4.Size = new System.Drawing.Size(866, 331);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Comport";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // cbModule
            // 
            this.cbModule.FormattingEnabled = true;
            this.cbModule.Location = new System.Drawing.Point(90, 46);
            this.cbModule.Name = "cbModule";
            this.cbModule.Size = new System.Drawing.Size(117, 21);
            this.cbModule.TabIndex = 29;
            // 
            // lblModuleComport
            // 
            this.lblModuleComport.AutoSize = true;
            this.lblModuleComport.Location = new System.Drawing.Point(12, 49);
            this.lblModuleComport.Name = "lblModuleComport";
            this.lblModuleComport.Size = new System.Drawing.Size(42, 13);
            this.lblModuleComport.TabIndex = 23;
            this.lblModuleComport.Text = "Module";
            // 
            // tabPage5
            // 
            this.tabPage5.AutoScroll = true;
            this.tabPage5.Controls.Add(this.var18ErrorMax);
            this.tabPage5.Controls.Add(this.var17ErrorMax);
            this.tabPage5.Controls.Add(this.var16ErrorMax);
            this.tabPage5.Controls.Add(this.var15ErrorMax);
            this.tabPage5.Controls.Add(this.var14ErrorMax);
            this.tabPage5.Controls.Add(this.var13ErrorMax);
            this.tabPage5.Controls.Add(this.var12ErrorMax);
            this.tabPage5.Controls.Add(this.var11ErrorMax);
            this.tabPage5.Controls.Add(this.var10ErrorMax);
            this.tabPage5.Controls.Add(this.var9ErrorMax);
            this.tabPage5.Controls.Add(this.var8ErrorMax);
            this.tabPage5.Controls.Add(this.var7ErrorMax);
            this.tabPage5.Controls.Add(this.var6ErrorMax);
            this.tabPage5.Controls.Add(this.var5ErrorMax);
            this.tabPage5.Controls.Add(this.var4ErrorMax);
            this.tabPage5.Controls.Add(this.var3ErrorMax);
            this.tabPage5.Controls.Add(this.var2ErrorMax);
            this.tabPage5.Controls.Add(this.var1ErrorMax);
            this.tabPage5.Controls.Add(this.label1);
            this.tabPage5.Controls.Add(this.var18DisplayName);
            this.tabPage5.Controls.Add(this.var17DisplayName);
            this.tabPage5.Controls.Add(this.var16DisplayName);
            this.tabPage5.Controls.Add(this.var15DisplayName);
            this.tabPage5.Controls.Add(this.var14DisplayName);
            this.tabPage5.Controls.Add(this.var13DisplayName);
            this.tabPage5.Controls.Add(this.var18Type);
            this.tabPage5.Controls.Add(this.var17Type);
            this.tabPage5.Controls.Add(this.var16Type);
            this.tabPage5.Controls.Add(this.var15Type);
            this.tabPage5.Controls.Add(this.var14Type);
            this.tabPage5.Controls.Add(this.var13Type);
            this.tabPage5.Controls.Add(this.var18StatusColumn);
            this.tabPage5.Controls.Add(this.var17StatusColumn);
            this.tabPage5.Controls.Add(this.var16StatusColumn);
            this.tabPage5.Controls.Add(this.var15StatusColumn);
            this.tabPage5.Controls.Add(this.var14StatusColumn);
            this.tabPage5.Controls.Add(this.var13StatusColumn);
            this.tabPage5.Controls.Add(this.var18ValueColumn);
            this.tabPage5.Controls.Add(this.var18Unit);
            this.tabPage5.Controls.Add(this.var17ValueColumn);
            this.tabPage5.Controls.Add(this.var17Unit);
            this.tabPage5.Controls.Add(this.var16ValueColumn);
            this.tabPage5.Controls.Add(this.var16Unit);
            this.tabPage5.Controls.Add(this.var15ValueColumn);
            this.tabPage5.Controls.Add(this.var15Unit);
            this.tabPage5.Controls.Add(this.var14ValueColumn);
            this.tabPage5.Controls.Add(this.var14Unit);
            this.tabPage5.Controls.Add(this.var13ValueColumn);
            this.tabPage5.Controls.Add(this.var13Unit);
            this.tabPage5.Controls.Add(this.var18Offset);
            this.tabPage5.Controls.Add(this.var17Offset);
            this.tabPage5.Controls.Add(this.var16Offset);
            this.tabPage5.Controls.Add(this.var15Offset);
            this.tabPage5.Controls.Add(this.var14Offset);
            this.tabPage5.Controls.Add(this.var13Offset);
            this.tabPage5.Controls.Add(this.var18OutputMax);
            this.tabPage5.Controls.Add(this.var18OutputMin);
            this.tabPage5.Controls.Add(this.var18InputMax);
            this.tabPage5.Controls.Add(this.var18InputMin);
            this.tabPage5.Controls.Add(this.var18Channel);
            this.tabPage5.Controls.Add(this.var17OutputMax);
            this.tabPage5.Controls.Add(this.var17OutputMin);
            this.tabPage5.Controls.Add(this.var17InputMax);
            this.tabPage5.Controls.Add(this.var17InputMin);
            this.tabPage5.Controls.Add(this.var17Channel);
            this.tabPage5.Controls.Add(this.var16OutputMax);
            this.tabPage5.Controls.Add(this.var16OutputMin);
            this.tabPage5.Controls.Add(this.var16InputMax);
            this.tabPage5.Controls.Add(this.var16InputMin);
            this.tabPage5.Controls.Add(this.var16Channel);
            this.tabPage5.Controls.Add(this.var15OutputMax);
            this.tabPage5.Controls.Add(this.var15OutputMin);
            this.tabPage5.Controls.Add(this.var15InputMax);
            this.tabPage5.Controls.Add(this.var15InputMin);
            this.tabPage5.Controls.Add(this.var15Channel);
            this.tabPage5.Controls.Add(this.var14OutputMax);
            this.tabPage5.Controls.Add(this.var14OutputMin);
            this.tabPage5.Controls.Add(this.var14InputMax);
            this.tabPage5.Controls.Add(this.var14InputMin);
            this.tabPage5.Controls.Add(this.var14Channel);
            this.tabPage5.Controls.Add(this.var13OutputMax);
            this.tabPage5.Controls.Add(this.var13OutputMin);
            this.tabPage5.Controls.Add(this.var13InputMax);
            this.tabPage5.Controls.Add(this.var13InputMin);
            this.tabPage5.Controls.Add(this.var13Channel);
            this.tabPage5.Controls.Add(this.var12DisplayName);
            this.tabPage5.Controls.Add(this.var11DisplayName);
            this.tabPage5.Controls.Add(this.var10DisplayName);
            this.tabPage5.Controls.Add(this.var9DisplayName);
            this.tabPage5.Controls.Add(this.var8DisplayName);
            this.tabPage5.Controls.Add(this.var7DisplayName);
            this.tabPage5.Controls.Add(this.var12Type);
            this.tabPage5.Controls.Add(this.var11Type);
            this.tabPage5.Controls.Add(this.var10Type);
            this.tabPage5.Controls.Add(this.var9Type);
            this.tabPage5.Controls.Add(this.var8Type);
            this.tabPage5.Controls.Add(this.var7Type);
            this.tabPage5.Controls.Add(this.var12StatusColumn);
            this.tabPage5.Controls.Add(this.var11StatusColumn);
            this.tabPage5.Controls.Add(this.var10StatusColumn);
            this.tabPage5.Controls.Add(this.var9StatusColumn);
            this.tabPage5.Controls.Add(this.var8StatusColumn);
            this.tabPage5.Controls.Add(this.var7StatusColumn);
            this.tabPage5.Controls.Add(this.var12ValueColumn);
            this.tabPage5.Controls.Add(this.var12Unit);
            this.tabPage5.Controls.Add(this.var11ValueColumn);
            this.tabPage5.Controls.Add(this.var11Unit);
            this.tabPage5.Controls.Add(this.var10ValueColumn);
            this.tabPage5.Controls.Add(this.var10Unit);
            this.tabPage5.Controls.Add(this.var9ValueColumn);
            this.tabPage5.Controls.Add(this.var9Unit);
            this.tabPage5.Controls.Add(this.var8ValueColumn);
            this.tabPage5.Controls.Add(this.var8Unit);
            this.tabPage5.Controls.Add(this.var7ValueColumn);
            this.tabPage5.Controls.Add(this.var7Unit);
            this.tabPage5.Controls.Add(this.var12Offset);
            this.tabPage5.Controls.Add(this.var11Offset);
            this.tabPage5.Controls.Add(this.var10Offset);
            this.tabPage5.Controls.Add(this.var9Offset);
            this.tabPage5.Controls.Add(this.var8Offset);
            this.tabPage5.Controls.Add(this.var7Offset);
            this.tabPage5.Controls.Add(this.var12OutputMax);
            this.tabPage5.Controls.Add(this.var12OutputMin);
            this.tabPage5.Controls.Add(this.var12InputMax);
            this.tabPage5.Controls.Add(this.var12InputMin);
            this.tabPage5.Controls.Add(this.var12Channel);
            this.tabPage5.Controls.Add(this.var11OutputMax);
            this.tabPage5.Controls.Add(this.var11OutputMin);
            this.tabPage5.Controls.Add(this.var11InputMax);
            this.tabPage5.Controls.Add(this.var11InputMin);
            this.tabPage5.Controls.Add(this.var11Channel);
            this.tabPage5.Controls.Add(this.var10OutputMax);
            this.tabPage5.Controls.Add(this.var10OutputMin);
            this.tabPage5.Controls.Add(this.var10InputMax);
            this.tabPage5.Controls.Add(this.var10InputMin);
            this.tabPage5.Controls.Add(this.var10Channel);
            this.tabPage5.Controls.Add(this.var9OutputMax);
            this.tabPage5.Controls.Add(this.var9OutputMin);
            this.tabPage5.Controls.Add(this.var9InputMax);
            this.tabPage5.Controls.Add(this.var9InputMin);
            this.tabPage5.Controls.Add(this.var9Channel);
            this.tabPage5.Controls.Add(this.var8OutputMax);
            this.tabPage5.Controls.Add(this.var8OutputMin);
            this.tabPage5.Controls.Add(this.var8InputMax);
            this.tabPage5.Controls.Add(this.var8InputMin);
            this.tabPage5.Controls.Add(this.var8Channel);
            this.tabPage5.Controls.Add(this.var7OutputMax);
            this.tabPage5.Controls.Add(this.var7OutputMin);
            this.tabPage5.Controls.Add(this.var7InputMax);
            this.tabPage5.Controls.Add(this.var7InputMin);
            this.tabPage5.Controls.Add(this.var7Channel);
            this.tabPage5.Controls.Add(this.var6DisplayName);
            this.tabPage5.Controls.Add(this.var5DisplayName);
            this.tabPage5.Controls.Add(this.var4DisplayName);
            this.tabPage5.Controls.Add(this.var3DisplayName);
            this.tabPage5.Controls.Add(this.var2DisplayName);
            this.tabPage5.Controls.Add(this.var1DisplayName);
            this.tabPage5.Controls.Add(this.var6Type);
            this.tabPage5.Controls.Add(this.var5Type);
            this.tabPage5.Controls.Add(this.var4Type);
            this.tabPage5.Controls.Add(this.var3Type);
            this.tabPage5.Controls.Add(this.var2Type);
            this.tabPage5.Controls.Add(this.var1Type);
            this.tabPage5.Controls.Add(this.var6StatusColumn);
            this.tabPage5.Controls.Add(this.var5StatusColumn);
            this.tabPage5.Controls.Add(this.var4StatusColumn);
            this.tabPage5.Controls.Add(this.var3StatusColumn);
            this.tabPage5.Controls.Add(this.var2StatusColumn);
            this.tabPage5.Controls.Add(this.var1StatusColumn);
            this.tabPage5.Controls.Add(this.var6ValueColumn);
            this.tabPage5.Controls.Add(this.var6Unit);
            this.tabPage5.Controls.Add(this.var5ValueColumn);
            this.tabPage5.Controls.Add(this.var5Unit);
            this.tabPage5.Controls.Add(this.var4ValueColumn);
            this.tabPage5.Controls.Add(this.var4Unit);
            this.tabPage5.Controls.Add(this.var3ValueColumn);
            this.tabPage5.Controls.Add(this.var3Unit);
            this.tabPage5.Controls.Add(this.var2ValueColumn);
            this.tabPage5.Controls.Add(this.var2Unit);
            this.tabPage5.Controls.Add(this.var1ValueColumn);
            this.tabPage5.Controls.Add(this.var1Unit);
            this.tabPage5.Controls.Add(this.var6Offset);
            this.tabPage5.Controls.Add(this.var5Offset);
            this.tabPage5.Controls.Add(this.var4Offset);
            this.tabPage5.Controls.Add(this.var3Offset);
            this.tabPage5.Controls.Add(this.var2Offset);
            this.tabPage5.Controls.Add(this.var1Offset);
            this.tabPage5.Controls.Add(this.var6OutputMax);
            this.tabPage5.Controls.Add(this.var6OutputMin);
            this.tabPage5.Controls.Add(this.var6InputMax);
            this.tabPage5.Controls.Add(this.var6InputMin);
            this.tabPage5.Controls.Add(this.var6Channel);
            this.tabPage5.Controls.Add(this.var5OutputMax);
            this.tabPage5.Controls.Add(this.var5OutputMin);
            this.tabPage5.Controls.Add(this.var5InputMax);
            this.tabPage5.Controls.Add(this.var5InputMin);
            this.tabPage5.Controls.Add(this.var5Channel);
            this.tabPage5.Controls.Add(this.var4OutputMax);
            this.tabPage5.Controls.Add(this.var4OutputMin);
            this.tabPage5.Controls.Add(this.var4InputMax);
            this.tabPage5.Controls.Add(this.var4InputMin);
            this.tabPage5.Controls.Add(this.var4Channel);
            this.tabPage5.Controls.Add(this.var3OutputMax);
            this.tabPage5.Controls.Add(this.var3OutputMin);
            this.tabPage5.Controls.Add(this.var3InputMax);
            this.tabPage5.Controls.Add(this.var3InputMin);
            this.tabPage5.Controls.Add(this.var3Channel);
            this.tabPage5.Controls.Add(this.var2OutputMax);
            this.tabPage5.Controls.Add(this.var2OutputMin);
            this.tabPage5.Controls.Add(this.var2InputMax);
            this.tabPage5.Controls.Add(this.var2InputMin);
            this.tabPage5.Controls.Add(this.var2Channel);
            this.tabPage5.Controls.Add(this.var1OutputMax);
            this.tabPage5.Controls.Add(this.var1OutputMin);
            this.tabPage5.Controls.Add(this.var1InputMax);
            this.tabPage5.Controls.Add(this.var1InputMin);
            this.tabPage5.Controls.Add(this.var1Channel);
            this.tabPage5.Controls.Add(this.var18Module);
            this.tabPage5.Controls.Add(this.var17Module);
            this.tabPage5.Controls.Add(this.var18);
            this.tabPage5.Controls.Add(this.var17);
            this.tabPage5.Controls.Add(this.var16Module);
            this.tabPage5.Controls.Add(this.var15Module);
            this.tabPage5.Controls.Add(this.var16);
            this.tabPage5.Controls.Add(this.var15);
            this.tabPage5.Controls.Add(this.var14Module);
            this.tabPage5.Controls.Add(this.var13Module);
            this.tabPage5.Controls.Add(this.var14);
            this.tabPage5.Controls.Add(this.var13);
            this.tabPage5.Controls.Add(this.var12Module);
            this.tabPage5.Controls.Add(this.var11Module);
            this.tabPage5.Controls.Add(this.var12);
            this.tabPage5.Controls.Add(this.var11);
            this.tabPage5.Controls.Add(this.var10Module);
            this.tabPage5.Controls.Add(this.var9Module);
            this.tabPage5.Controls.Add(this.var10);
            this.tabPage5.Controls.Add(this.var9);
            this.tabPage5.Controls.Add(this.var8Module);
            this.tabPage5.Controls.Add(this.var7Module);
            this.tabPage5.Controls.Add(this.var8);
            this.tabPage5.Controls.Add(this.var7);
            this.tabPage5.Controls.Add(this.label55);
            this.tabPage5.Controls.Add(this.label54);
            this.tabPage5.Controls.Add(this.label52);
            this.tabPage5.Controls.Add(this.label53);
            this.tabPage5.Controls.Add(this.Unit);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.var6Module);
            this.tabPage5.Controls.Add(this.var5Module);
            this.tabPage5.Controls.Add(this.var6);
            this.tabPage5.Controls.Add(this.var5);
            this.tabPage5.Controls.Add(this.var4Module);
            this.tabPage5.Controls.Add(this.var3Module);
            this.tabPage5.Controls.Add(this.var4);
            this.tabPage5.Controls.Add(this.var3);
            this.tabPage5.Controls.Add(this.var2Module);
            this.tabPage5.Controls.Add(this.var1Module);
            this.tabPage5.Controls.Add(this.var2);
            this.tabPage5.Controls.Add(this.var1);
            this.tabPage5.Controls.Add(this.label8);
            this.tabPage5.Controls.Add(this.label9);
            this.tabPage5.Controls.Add(this.label7);
            this.tabPage5.Controls.Add(this.label6);
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage5.Size = new System.Drawing.Size(866, 331);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "MPS";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // var18ErrorMax
            // 
            this.var18ErrorMax.Location = new System.Drawing.Point(1004, 491);
            this.var18ErrorMax.Name = "var18ErrorMax";
            this.var18ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var18ErrorMax.TabIndex = 568;
            this.var18ErrorMax.Visible = false;
            // 
            // var17ErrorMax
            // 
            this.var17ErrorMax.Location = new System.Drawing.Point(1004, 465);
            this.var17ErrorMax.Name = "var17ErrorMax";
            this.var17ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var17ErrorMax.TabIndex = 567;
            this.var17ErrorMax.Visible = false;
            // 
            // var16ErrorMax
            // 
            this.var16ErrorMax.Location = new System.Drawing.Point(1004, 440);
            this.var16ErrorMax.Name = "var16ErrorMax";
            this.var16ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var16ErrorMax.TabIndex = 566;
            this.var16ErrorMax.Visible = false;
            // 
            // var15ErrorMax
            // 
            this.var15ErrorMax.Location = new System.Drawing.Point(1004, 413);
            this.var15ErrorMax.Name = "var15ErrorMax";
            this.var15ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var15ErrorMax.TabIndex = 565;
            this.var15ErrorMax.Visible = false;
            // 
            // var14ErrorMax
            // 
            this.var14ErrorMax.Location = new System.Drawing.Point(1004, 389);
            this.var14ErrorMax.Name = "var14ErrorMax";
            this.var14ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var14ErrorMax.TabIndex = 564;
            this.var14ErrorMax.Visible = false;
            // 
            // var13ErrorMax
            // 
            this.var13ErrorMax.Location = new System.Drawing.Point(1004, 362);
            this.var13ErrorMax.Name = "var13ErrorMax";
            this.var13ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var13ErrorMax.TabIndex = 563;
            // 
            // var12ErrorMax
            // 
            this.var12ErrorMax.Location = new System.Drawing.Point(1004, 335);
            this.var12ErrorMax.Name = "var12ErrorMax";
            this.var12ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var12ErrorMax.TabIndex = 562;
            // 
            // var11ErrorMax
            // 
            this.var11ErrorMax.Location = new System.Drawing.Point(1004, 309);
            this.var11ErrorMax.Name = "var11ErrorMax";
            this.var11ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var11ErrorMax.TabIndex = 561;
            // 
            // var10ErrorMax
            // 
            this.var10ErrorMax.Location = new System.Drawing.Point(1004, 284);
            this.var10ErrorMax.Name = "var10ErrorMax";
            this.var10ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var10ErrorMax.TabIndex = 560;
            // 
            // var9ErrorMax
            // 
            this.var9ErrorMax.Location = new System.Drawing.Point(1004, 257);
            this.var9ErrorMax.Name = "var9ErrorMax";
            this.var9ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var9ErrorMax.TabIndex = 559;
            // 
            // var8ErrorMax
            // 
            this.var8ErrorMax.Location = new System.Drawing.Point(1004, 233);
            this.var8ErrorMax.Name = "var8ErrorMax";
            this.var8ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var8ErrorMax.TabIndex = 558;
            // 
            // var7ErrorMax
            // 
            this.var7ErrorMax.Location = new System.Drawing.Point(1004, 206);
            this.var7ErrorMax.Name = "var7ErrorMax";
            this.var7ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var7ErrorMax.TabIndex = 557;
            // 
            // var6ErrorMax
            // 
            this.var6ErrorMax.Location = new System.Drawing.Point(1004, 179);
            this.var6ErrorMax.Name = "var6ErrorMax";
            this.var6ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var6ErrorMax.TabIndex = 556;
            // 
            // var5ErrorMax
            // 
            this.var5ErrorMax.Location = new System.Drawing.Point(1004, 153);
            this.var5ErrorMax.Name = "var5ErrorMax";
            this.var5ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var5ErrorMax.TabIndex = 555;
            // 
            // var4ErrorMax
            // 
            this.var4ErrorMax.Location = new System.Drawing.Point(1004, 128);
            this.var4ErrorMax.Name = "var4ErrorMax";
            this.var4ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var4ErrorMax.TabIndex = 554;
            // 
            // var3ErrorMax
            // 
            this.var3ErrorMax.Location = new System.Drawing.Point(1004, 101);
            this.var3ErrorMax.Name = "var3ErrorMax";
            this.var3ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var3ErrorMax.TabIndex = 553;
            // 
            // var2ErrorMax
            // 
            this.var2ErrorMax.Location = new System.Drawing.Point(1004, 77);
            this.var2ErrorMax.Name = "var2ErrorMax";
            this.var2ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var2ErrorMax.TabIndex = 552;
            // 
            // var1ErrorMax
            // 
            this.var1ErrorMax.Location = new System.Drawing.Point(1004, 50);
            this.var1ErrorMax.Name = "var1ErrorMax";
            this.var1ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var1ErrorMax.TabIndex = 551;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1005, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 550;
            this.label1.Text = "Alarm";
            // 
            // var18DisplayName
            // 
            this.var18DisplayName.Location = new System.Drawing.Point(923, 491);
            this.var18DisplayName.Name = "var18DisplayName";
            this.var18DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var18DisplayName.TabIndex = 549;
            this.var18DisplayName.Visible = false;
            // 
            // var17DisplayName
            // 
            this.var17DisplayName.Location = new System.Drawing.Point(923, 465);
            this.var17DisplayName.Name = "var17DisplayName";
            this.var17DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var17DisplayName.TabIndex = 548;
            this.var17DisplayName.Visible = false;
            // 
            // var16DisplayName
            // 
            this.var16DisplayName.Location = new System.Drawing.Point(923, 440);
            this.var16DisplayName.Name = "var16DisplayName";
            this.var16DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var16DisplayName.TabIndex = 547;
            this.var16DisplayName.Visible = false;
            // 
            // var15DisplayName
            // 
            this.var15DisplayName.Location = new System.Drawing.Point(923, 413);
            this.var15DisplayName.Name = "var15DisplayName";
            this.var15DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var15DisplayName.TabIndex = 546;
            this.var15DisplayName.Visible = false;
            // 
            // var14DisplayName
            // 
            this.var14DisplayName.Location = new System.Drawing.Point(923, 389);
            this.var14DisplayName.Name = "var14DisplayName";
            this.var14DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var14DisplayName.TabIndex = 545;
            this.var14DisplayName.Visible = false;
            // 
            // var13DisplayName
            // 
            this.var13DisplayName.Location = new System.Drawing.Point(923, 362);
            this.var13DisplayName.Name = "var13DisplayName";
            this.var13DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var13DisplayName.TabIndex = 544;
            // 
            // var18Type
            // 
            this.var18Type.Location = new System.Drawing.Point(845, 491);
            this.var18Type.Name = "var18Type";
            this.var18Type.Size = new System.Drawing.Size(63, 20);
            this.var18Type.TabIndex = 543;
            this.var18Type.Visible = false;
            // 
            // var17Type
            // 
            this.var17Type.Location = new System.Drawing.Point(845, 465);
            this.var17Type.Name = "var17Type";
            this.var17Type.Size = new System.Drawing.Size(63, 20);
            this.var17Type.TabIndex = 542;
            this.var17Type.Visible = false;
            // 
            // var16Type
            // 
            this.var16Type.Location = new System.Drawing.Point(845, 440);
            this.var16Type.Name = "var16Type";
            this.var16Type.Size = new System.Drawing.Size(63, 20);
            this.var16Type.TabIndex = 541;
            this.var16Type.Visible = false;
            // 
            // var15Type
            // 
            this.var15Type.Location = new System.Drawing.Point(845, 413);
            this.var15Type.Name = "var15Type";
            this.var15Type.Size = new System.Drawing.Size(63, 20);
            this.var15Type.TabIndex = 540;
            this.var15Type.Visible = false;
            // 
            // var14Type
            // 
            this.var14Type.Location = new System.Drawing.Point(845, 389);
            this.var14Type.Name = "var14Type";
            this.var14Type.Size = new System.Drawing.Size(63, 20);
            this.var14Type.TabIndex = 539;
            this.var14Type.Visible = false;
            // 
            // var13Type
            // 
            this.var13Type.Location = new System.Drawing.Point(845, 362);
            this.var13Type.Name = "var13Type";
            this.var13Type.Size = new System.Drawing.Size(63, 20);
            this.var13Type.TabIndex = 538;
            // 
            // var18StatusColumn
            // 
            this.var18StatusColumn.Enabled = false;
            this.var18StatusColumn.Location = new System.Drawing.Point(761, 491);
            this.var18StatusColumn.Name = "var18StatusColumn";
            this.var18StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var18StatusColumn.TabIndex = 537;
            this.var18StatusColumn.Visible = false;
            // 
            // var17StatusColumn
            // 
            this.var17StatusColumn.Enabled = false;
            this.var17StatusColumn.Location = new System.Drawing.Point(761, 465);
            this.var17StatusColumn.Name = "var17StatusColumn";
            this.var17StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var17StatusColumn.TabIndex = 536;
            this.var17StatusColumn.Visible = false;
            // 
            // var16StatusColumn
            // 
            this.var16StatusColumn.Enabled = false;
            this.var16StatusColumn.Location = new System.Drawing.Point(761, 440);
            this.var16StatusColumn.Name = "var16StatusColumn";
            this.var16StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var16StatusColumn.TabIndex = 535;
            this.var16StatusColumn.Visible = false;
            // 
            // var15StatusColumn
            // 
            this.var15StatusColumn.Enabled = false;
            this.var15StatusColumn.Location = new System.Drawing.Point(761, 413);
            this.var15StatusColumn.Name = "var15StatusColumn";
            this.var15StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var15StatusColumn.TabIndex = 534;
            this.var15StatusColumn.Visible = false;
            // 
            // var14StatusColumn
            // 
            this.var14StatusColumn.Enabled = false;
            this.var14StatusColumn.Location = new System.Drawing.Point(761, 389);
            this.var14StatusColumn.Name = "var14StatusColumn";
            this.var14StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var14StatusColumn.TabIndex = 533;
            this.var14StatusColumn.Visible = false;
            // 
            // var13StatusColumn
            // 
            this.var13StatusColumn.Enabled = false;
            this.var13StatusColumn.Location = new System.Drawing.Point(761, 362);
            this.var13StatusColumn.Name = "var13StatusColumn";
            this.var13StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var13StatusColumn.TabIndex = 532;
            // 
            // var18ValueColumn
            // 
            this.var18ValueColumn.Enabled = false;
            this.var18ValueColumn.Location = new System.Drawing.Point(680, 491);
            this.var18ValueColumn.Name = "var18ValueColumn";
            this.var18ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var18ValueColumn.TabIndex = 531;
            this.var18ValueColumn.Visible = false;
            // 
            // var18Unit
            // 
            this.var18Unit.Location = new System.Drawing.Point(603, 491);
            this.var18Unit.Name = "var18Unit";
            this.var18Unit.Size = new System.Drawing.Size(63, 20);
            this.var18Unit.TabIndex = 530;
            this.var18Unit.Visible = false;
            // 
            // var17ValueColumn
            // 
            this.var17ValueColumn.Enabled = false;
            this.var17ValueColumn.Location = new System.Drawing.Point(680, 464);
            this.var17ValueColumn.Name = "var17ValueColumn";
            this.var17ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var17ValueColumn.TabIndex = 529;
            this.var17ValueColumn.Visible = false;
            // 
            // var17Unit
            // 
            this.var17Unit.Location = new System.Drawing.Point(603, 464);
            this.var17Unit.Name = "var17Unit";
            this.var17Unit.Size = new System.Drawing.Size(63, 20);
            this.var17Unit.TabIndex = 528;
            this.var17Unit.Visible = false;
            // 
            // var16ValueColumn
            // 
            this.var16ValueColumn.Enabled = false;
            this.var16ValueColumn.Location = new System.Drawing.Point(680, 439);
            this.var16ValueColumn.Name = "var16ValueColumn";
            this.var16ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var16ValueColumn.TabIndex = 527;
            this.var16ValueColumn.Visible = false;
            // 
            // var16Unit
            // 
            this.var16Unit.Location = new System.Drawing.Point(603, 439);
            this.var16Unit.Name = "var16Unit";
            this.var16Unit.Size = new System.Drawing.Size(63, 20);
            this.var16Unit.TabIndex = 526;
            this.var16Unit.Visible = false;
            // 
            // var15ValueColumn
            // 
            this.var15ValueColumn.Enabled = false;
            this.var15ValueColumn.Location = new System.Drawing.Point(680, 413);
            this.var15ValueColumn.Name = "var15ValueColumn";
            this.var15ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var15ValueColumn.TabIndex = 525;
            this.var15ValueColumn.Visible = false;
            // 
            // var15Unit
            // 
            this.var15Unit.Location = new System.Drawing.Point(603, 413);
            this.var15Unit.Name = "var15Unit";
            this.var15Unit.Size = new System.Drawing.Size(63, 20);
            this.var15Unit.TabIndex = 524;
            this.var15Unit.Visible = false;
            // 
            // var14ValueColumn
            // 
            this.var14ValueColumn.Enabled = false;
            this.var14ValueColumn.Location = new System.Drawing.Point(680, 388);
            this.var14ValueColumn.Name = "var14ValueColumn";
            this.var14ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var14ValueColumn.TabIndex = 523;
            this.var14ValueColumn.Visible = false;
            // 
            // var14Unit
            // 
            this.var14Unit.Location = new System.Drawing.Point(603, 388);
            this.var14Unit.Name = "var14Unit";
            this.var14Unit.Size = new System.Drawing.Size(63, 20);
            this.var14Unit.TabIndex = 522;
            this.var14Unit.Visible = false;
            // 
            // var13ValueColumn
            // 
            this.var13ValueColumn.Enabled = false;
            this.var13ValueColumn.Location = new System.Drawing.Point(680, 361);
            this.var13ValueColumn.Name = "var13ValueColumn";
            this.var13ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var13ValueColumn.TabIndex = 521;
            // 
            // var13Unit
            // 
            this.var13Unit.Location = new System.Drawing.Point(603, 361);
            this.var13Unit.Name = "var13Unit";
            this.var13Unit.Size = new System.Drawing.Size(63, 20);
            this.var13Unit.TabIndex = 520;
            // 
            // var18Offset
            // 
            this.var18Offset.Location = new System.Drawing.Point(522, 491);
            this.var18Offset.Name = "var18Offset";
            this.var18Offset.Size = new System.Drawing.Size(63, 20);
            this.var18Offset.TabIndex = 519;
            this.var18Offset.Visible = false;
            // 
            // var17Offset
            // 
            this.var17Offset.Location = new System.Drawing.Point(522, 465);
            this.var17Offset.Name = "var17Offset";
            this.var17Offset.Size = new System.Drawing.Size(63, 20);
            this.var17Offset.TabIndex = 518;
            this.var17Offset.Visible = false;
            // 
            // var16Offset
            // 
            this.var16Offset.Location = new System.Drawing.Point(522, 440);
            this.var16Offset.Name = "var16Offset";
            this.var16Offset.Size = new System.Drawing.Size(63, 20);
            this.var16Offset.TabIndex = 517;
            this.var16Offset.Visible = false;
            // 
            // var15Offset
            // 
            this.var15Offset.Location = new System.Drawing.Point(522, 413);
            this.var15Offset.Name = "var15Offset";
            this.var15Offset.Size = new System.Drawing.Size(63, 20);
            this.var15Offset.TabIndex = 516;
            this.var15Offset.Visible = false;
            // 
            // var14Offset
            // 
            this.var14Offset.Location = new System.Drawing.Point(522, 389);
            this.var14Offset.Name = "var14Offset";
            this.var14Offset.Size = new System.Drawing.Size(63, 20);
            this.var14Offset.TabIndex = 515;
            this.var14Offset.Visible = false;
            // 
            // var13Offset
            // 
            this.var13Offset.Location = new System.Drawing.Point(522, 362);
            this.var13Offset.Name = "var13Offset";
            this.var13Offset.Size = new System.Drawing.Size(63, 20);
            this.var13Offset.TabIndex = 514;
            // 
            // var18OutputMax
            // 
            this.var18OutputMax.Location = new System.Drawing.Point(441, 491);
            this.var18OutputMax.Name = "var18OutputMax";
            this.var18OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var18OutputMax.TabIndex = 513;
            this.var18OutputMax.Visible = false;
            // 
            // var18OutputMin
            // 
            this.var18OutputMin.Location = new System.Drawing.Point(364, 491);
            this.var18OutputMin.Name = "var18OutputMin";
            this.var18OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var18OutputMin.TabIndex = 512;
            this.var18OutputMin.Visible = false;
            // 
            // var18InputMax
            // 
            this.var18InputMax.Location = new System.Drawing.Point(286, 491);
            this.var18InputMax.Name = "var18InputMax";
            this.var18InputMax.Size = new System.Drawing.Size(63, 20);
            this.var18InputMax.TabIndex = 511;
            this.var18InputMax.Visible = false;
            // 
            // var18InputMin
            // 
            this.var18InputMin.Location = new System.Drawing.Point(210, 490);
            this.var18InputMin.Name = "var18InputMin";
            this.var18InputMin.Size = new System.Drawing.Size(63, 20);
            this.var18InputMin.TabIndex = 510;
            this.var18InputMin.Visible = false;
            // 
            // var18Channel
            // 
            this.var18Channel.Location = new System.Drawing.Point(129, 491);
            this.var18Channel.Name = "var18Channel";
            this.var18Channel.Size = new System.Drawing.Size(63, 20);
            this.var18Channel.TabIndex = 508;
            this.var18Channel.Visible = false;
            // 
            // var17OutputMax
            // 
            this.var17OutputMax.Location = new System.Drawing.Point(441, 464);
            this.var17OutputMax.Name = "var17OutputMax";
            this.var17OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var17OutputMax.TabIndex = 507;
            this.var17OutputMax.Visible = false;
            // 
            // var17OutputMin
            // 
            this.var17OutputMin.Location = new System.Drawing.Point(364, 464);
            this.var17OutputMin.Name = "var17OutputMin";
            this.var17OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var17OutputMin.TabIndex = 506;
            this.var17OutputMin.Visible = false;
            // 
            // var17InputMax
            // 
            this.var17InputMax.Location = new System.Drawing.Point(286, 464);
            this.var17InputMax.Name = "var17InputMax";
            this.var17InputMax.Size = new System.Drawing.Size(63, 20);
            this.var17InputMax.TabIndex = 505;
            this.var17InputMax.Visible = false;
            // 
            // var17InputMin
            // 
            this.var17InputMin.Location = new System.Drawing.Point(210, 463);
            this.var17InputMin.Name = "var17InputMin";
            this.var17InputMin.Size = new System.Drawing.Size(63, 20);
            this.var17InputMin.TabIndex = 504;
            this.var17InputMin.Visible = false;
            // 
            // var17Channel
            // 
            this.var17Channel.Location = new System.Drawing.Point(129, 464);
            this.var17Channel.Name = "var17Channel";
            this.var17Channel.Size = new System.Drawing.Size(63, 20);
            this.var17Channel.TabIndex = 502;
            this.var17Channel.Visible = false;
            // 
            // var16OutputMax
            // 
            this.var16OutputMax.Location = new System.Drawing.Point(441, 439);
            this.var16OutputMax.Name = "var16OutputMax";
            this.var16OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var16OutputMax.TabIndex = 499;
            this.var16OutputMax.Visible = false;
            // 
            // var16OutputMin
            // 
            this.var16OutputMin.Location = new System.Drawing.Point(364, 439);
            this.var16OutputMin.Name = "var16OutputMin";
            this.var16OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var16OutputMin.TabIndex = 498;
            this.var16OutputMin.Visible = false;
            // 
            // var16InputMax
            // 
            this.var16InputMax.Location = new System.Drawing.Point(286, 439);
            this.var16InputMax.Name = "var16InputMax";
            this.var16InputMax.Size = new System.Drawing.Size(63, 20);
            this.var16InputMax.TabIndex = 497;
            this.var16InputMax.Visible = false;
            // 
            // var16InputMin
            // 
            this.var16InputMin.Location = new System.Drawing.Point(210, 439);
            this.var16InputMin.Name = "var16InputMin";
            this.var16InputMin.Size = new System.Drawing.Size(63, 20);
            this.var16InputMin.TabIndex = 496;
            this.var16InputMin.Visible = false;
            // 
            // var16Channel
            // 
            this.var16Channel.Location = new System.Drawing.Point(129, 439);
            this.var16Channel.Name = "var16Channel";
            this.var16Channel.Size = new System.Drawing.Size(63, 20);
            this.var16Channel.TabIndex = 494;
            this.var16Channel.Visible = false;
            // 
            // var15OutputMax
            // 
            this.var15OutputMax.Location = new System.Drawing.Point(441, 413);
            this.var15OutputMax.Name = "var15OutputMax";
            this.var15OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var15OutputMax.TabIndex = 493;
            this.var15OutputMax.Visible = false;
            // 
            // var15OutputMin
            // 
            this.var15OutputMin.Location = new System.Drawing.Point(364, 413);
            this.var15OutputMin.Name = "var15OutputMin";
            this.var15OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var15OutputMin.TabIndex = 492;
            this.var15OutputMin.Visible = false;
            // 
            // var15InputMax
            // 
            this.var15InputMax.Location = new System.Drawing.Point(286, 413);
            this.var15InputMax.Name = "var15InputMax";
            this.var15InputMax.Size = new System.Drawing.Size(63, 20);
            this.var15InputMax.TabIndex = 491;
            this.var15InputMax.Visible = false;
            // 
            // var15InputMin
            // 
            this.var15InputMin.Location = new System.Drawing.Point(210, 412);
            this.var15InputMin.Name = "var15InputMin";
            this.var15InputMin.Size = new System.Drawing.Size(63, 20);
            this.var15InputMin.TabIndex = 490;
            this.var15InputMin.Visible = false;
            // 
            // var15Channel
            // 
            this.var15Channel.Location = new System.Drawing.Point(129, 413);
            this.var15Channel.Name = "var15Channel";
            this.var15Channel.Size = new System.Drawing.Size(63, 20);
            this.var15Channel.TabIndex = 488;
            this.var15Channel.Visible = false;
            // 
            // var14OutputMax
            // 
            this.var14OutputMax.Location = new System.Drawing.Point(441, 388);
            this.var14OutputMax.Name = "var14OutputMax";
            this.var14OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var14OutputMax.TabIndex = 485;
            this.var14OutputMax.Visible = false;
            // 
            // var14OutputMin
            // 
            this.var14OutputMin.Location = new System.Drawing.Point(364, 388);
            this.var14OutputMin.Name = "var14OutputMin";
            this.var14OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var14OutputMin.TabIndex = 484;
            this.var14OutputMin.Visible = false;
            // 
            // var14InputMax
            // 
            this.var14InputMax.Location = new System.Drawing.Point(286, 388);
            this.var14InputMax.Name = "var14InputMax";
            this.var14InputMax.Size = new System.Drawing.Size(63, 20);
            this.var14InputMax.TabIndex = 483;
            this.var14InputMax.Visible = false;
            // 
            // var14InputMin
            // 
            this.var14InputMin.Location = new System.Drawing.Point(210, 387);
            this.var14InputMin.Name = "var14InputMin";
            this.var14InputMin.Size = new System.Drawing.Size(63, 20);
            this.var14InputMin.TabIndex = 482;
            this.var14InputMin.Visible = false;
            // 
            // var14Channel
            // 
            this.var14Channel.Location = new System.Drawing.Point(129, 388);
            this.var14Channel.Name = "var14Channel";
            this.var14Channel.Size = new System.Drawing.Size(63, 20);
            this.var14Channel.TabIndex = 480;
            this.var14Channel.Visible = false;
            // 
            // var13OutputMax
            // 
            this.var13OutputMax.Location = new System.Drawing.Point(441, 361);
            this.var13OutputMax.Name = "var13OutputMax";
            this.var13OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var13OutputMax.TabIndex = 479;
            // 
            // var13OutputMin
            // 
            this.var13OutputMin.Location = new System.Drawing.Point(364, 361);
            this.var13OutputMin.Name = "var13OutputMin";
            this.var13OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var13OutputMin.TabIndex = 478;
            // 
            // var13InputMax
            // 
            this.var13InputMax.Location = new System.Drawing.Point(286, 361);
            this.var13InputMax.Name = "var13InputMax";
            this.var13InputMax.Size = new System.Drawing.Size(63, 20);
            this.var13InputMax.TabIndex = 477;
            // 
            // var13InputMin
            // 
            this.var13InputMin.Location = new System.Drawing.Point(210, 361);
            this.var13InputMin.Name = "var13InputMin";
            this.var13InputMin.Size = new System.Drawing.Size(63, 20);
            this.var13InputMin.TabIndex = 476;
            // 
            // var13Channel
            // 
            this.var13Channel.Location = new System.Drawing.Point(129, 361);
            this.var13Channel.Name = "var13Channel";
            this.var13Channel.Size = new System.Drawing.Size(63, 20);
            this.var13Channel.TabIndex = 474;
            // 
            // var12DisplayName
            // 
            this.var12DisplayName.Location = new System.Drawing.Point(923, 335);
            this.var12DisplayName.Name = "var12DisplayName";
            this.var12DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var12DisplayName.TabIndex = 471;
            // 
            // var11DisplayName
            // 
            this.var11DisplayName.Location = new System.Drawing.Point(923, 309);
            this.var11DisplayName.Name = "var11DisplayName";
            this.var11DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var11DisplayName.TabIndex = 470;
            // 
            // var10DisplayName
            // 
            this.var10DisplayName.Location = new System.Drawing.Point(923, 284);
            this.var10DisplayName.Name = "var10DisplayName";
            this.var10DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var10DisplayName.TabIndex = 469;
            // 
            // var9DisplayName
            // 
            this.var9DisplayName.Location = new System.Drawing.Point(923, 257);
            this.var9DisplayName.Name = "var9DisplayName";
            this.var9DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var9DisplayName.TabIndex = 468;
            // 
            // var8DisplayName
            // 
            this.var8DisplayName.Location = new System.Drawing.Point(923, 233);
            this.var8DisplayName.Name = "var8DisplayName";
            this.var8DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var8DisplayName.TabIndex = 467;
            // 
            // var7DisplayName
            // 
            this.var7DisplayName.Location = new System.Drawing.Point(923, 206);
            this.var7DisplayName.Name = "var7DisplayName";
            this.var7DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var7DisplayName.TabIndex = 466;
            // 
            // var12Type
            // 
            this.var12Type.Location = new System.Drawing.Point(845, 335);
            this.var12Type.Name = "var12Type";
            this.var12Type.Size = new System.Drawing.Size(63, 20);
            this.var12Type.TabIndex = 465;
            // 
            // var11Type
            // 
            this.var11Type.Location = new System.Drawing.Point(845, 309);
            this.var11Type.Name = "var11Type";
            this.var11Type.Size = new System.Drawing.Size(63, 20);
            this.var11Type.TabIndex = 464;
            // 
            // var10Type
            // 
            this.var10Type.Location = new System.Drawing.Point(845, 284);
            this.var10Type.Name = "var10Type";
            this.var10Type.Size = new System.Drawing.Size(63, 20);
            this.var10Type.TabIndex = 463;
            // 
            // var9Type
            // 
            this.var9Type.Location = new System.Drawing.Point(845, 257);
            this.var9Type.Name = "var9Type";
            this.var9Type.Size = new System.Drawing.Size(63, 20);
            this.var9Type.TabIndex = 462;
            // 
            // var8Type
            // 
            this.var8Type.Location = new System.Drawing.Point(845, 233);
            this.var8Type.Name = "var8Type";
            this.var8Type.Size = new System.Drawing.Size(63, 20);
            this.var8Type.TabIndex = 461;
            // 
            // var7Type
            // 
            this.var7Type.Location = new System.Drawing.Point(845, 206);
            this.var7Type.Name = "var7Type";
            this.var7Type.Size = new System.Drawing.Size(63, 20);
            this.var7Type.TabIndex = 460;
            // 
            // var12StatusColumn
            // 
            this.var12StatusColumn.Enabled = false;
            this.var12StatusColumn.Location = new System.Drawing.Point(761, 335);
            this.var12StatusColumn.Name = "var12StatusColumn";
            this.var12StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var12StatusColumn.TabIndex = 459;
            // 
            // var11StatusColumn
            // 
            this.var11StatusColumn.Enabled = false;
            this.var11StatusColumn.Location = new System.Drawing.Point(761, 309);
            this.var11StatusColumn.Name = "var11StatusColumn";
            this.var11StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var11StatusColumn.TabIndex = 458;
            // 
            // var10StatusColumn
            // 
            this.var10StatusColumn.Enabled = false;
            this.var10StatusColumn.Location = new System.Drawing.Point(761, 284);
            this.var10StatusColumn.Name = "var10StatusColumn";
            this.var10StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var10StatusColumn.TabIndex = 457;
            // 
            // var9StatusColumn
            // 
            this.var9StatusColumn.Enabled = false;
            this.var9StatusColumn.Location = new System.Drawing.Point(761, 257);
            this.var9StatusColumn.Name = "var9StatusColumn";
            this.var9StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var9StatusColumn.TabIndex = 456;
            // 
            // var8StatusColumn
            // 
            this.var8StatusColumn.Enabled = false;
            this.var8StatusColumn.Location = new System.Drawing.Point(761, 233);
            this.var8StatusColumn.Name = "var8StatusColumn";
            this.var8StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var8StatusColumn.TabIndex = 455;
            // 
            // var7StatusColumn
            // 
            this.var7StatusColumn.Enabled = false;
            this.var7StatusColumn.Location = new System.Drawing.Point(761, 206);
            this.var7StatusColumn.Name = "var7StatusColumn";
            this.var7StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var7StatusColumn.TabIndex = 454;
            // 
            // var12ValueColumn
            // 
            this.var12ValueColumn.Enabled = false;
            this.var12ValueColumn.Location = new System.Drawing.Point(680, 335);
            this.var12ValueColumn.Name = "var12ValueColumn";
            this.var12ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var12ValueColumn.TabIndex = 453;
            // 
            // var12Unit
            // 
            this.var12Unit.Location = new System.Drawing.Point(603, 335);
            this.var12Unit.Name = "var12Unit";
            this.var12Unit.Size = new System.Drawing.Size(63, 20);
            this.var12Unit.TabIndex = 452;
            // 
            // var11ValueColumn
            // 
            this.var11ValueColumn.Enabled = false;
            this.var11ValueColumn.Location = new System.Drawing.Point(680, 308);
            this.var11ValueColumn.Name = "var11ValueColumn";
            this.var11ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var11ValueColumn.TabIndex = 451;
            // 
            // var11Unit
            // 
            this.var11Unit.Location = new System.Drawing.Point(603, 308);
            this.var11Unit.Name = "var11Unit";
            this.var11Unit.Size = new System.Drawing.Size(63, 20);
            this.var11Unit.TabIndex = 450;
            // 
            // var10ValueColumn
            // 
            this.var10ValueColumn.Enabled = false;
            this.var10ValueColumn.Location = new System.Drawing.Point(680, 283);
            this.var10ValueColumn.Name = "var10ValueColumn";
            this.var10ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var10ValueColumn.TabIndex = 449;
            // 
            // var10Unit
            // 
            this.var10Unit.Location = new System.Drawing.Point(603, 283);
            this.var10Unit.Name = "var10Unit";
            this.var10Unit.Size = new System.Drawing.Size(63, 20);
            this.var10Unit.TabIndex = 448;
            // 
            // var9ValueColumn
            // 
            this.var9ValueColumn.Enabled = false;
            this.var9ValueColumn.Location = new System.Drawing.Point(680, 257);
            this.var9ValueColumn.Name = "var9ValueColumn";
            this.var9ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var9ValueColumn.TabIndex = 447;
            // 
            // var9Unit
            // 
            this.var9Unit.Location = new System.Drawing.Point(603, 257);
            this.var9Unit.Name = "var9Unit";
            this.var9Unit.Size = new System.Drawing.Size(63, 20);
            this.var9Unit.TabIndex = 446;
            // 
            // var8ValueColumn
            // 
            this.var8ValueColumn.Enabled = false;
            this.var8ValueColumn.Location = new System.Drawing.Point(680, 232);
            this.var8ValueColumn.Name = "var8ValueColumn";
            this.var8ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var8ValueColumn.TabIndex = 445;
            // 
            // var8Unit
            // 
            this.var8Unit.Location = new System.Drawing.Point(603, 232);
            this.var8Unit.Name = "var8Unit";
            this.var8Unit.Size = new System.Drawing.Size(63, 20);
            this.var8Unit.TabIndex = 444;
            // 
            // var7ValueColumn
            // 
            this.var7ValueColumn.Enabled = false;
            this.var7ValueColumn.Location = new System.Drawing.Point(680, 205);
            this.var7ValueColumn.Name = "var7ValueColumn";
            this.var7ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var7ValueColumn.TabIndex = 443;
            // 
            // var7Unit
            // 
            this.var7Unit.Location = new System.Drawing.Point(603, 205);
            this.var7Unit.Name = "var7Unit";
            this.var7Unit.Size = new System.Drawing.Size(63, 20);
            this.var7Unit.TabIndex = 442;
            // 
            // var12Offset
            // 
            this.var12Offset.Location = new System.Drawing.Point(522, 335);
            this.var12Offset.Name = "var12Offset";
            this.var12Offset.Size = new System.Drawing.Size(63, 20);
            this.var12Offset.TabIndex = 441;
            // 
            // var11Offset
            // 
            this.var11Offset.Location = new System.Drawing.Point(522, 309);
            this.var11Offset.Name = "var11Offset";
            this.var11Offset.Size = new System.Drawing.Size(63, 20);
            this.var11Offset.TabIndex = 440;
            // 
            // var10Offset
            // 
            this.var10Offset.Location = new System.Drawing.Point(522, 284);
            this.var10Offset.Name = "var10Offset";
            this.var10Offset.Size = new System.Drawing.Size(63, 20);
            this.var10Offset.TabIndex = 439;
            // 
            // var9Offset
            // 
            this.var9Offset.Location = new System.Drawing.Point(522, 257);
            this.var9Offset.Name = "var9Offset";
            this.var9Offset.Size = new System.Drawing.Size(63, 20);
            this.var9Offset.TabIndex = 438;
            // 
            // var8Offset
            // 
            this.var8Offset.Location = new System.Drawing.Point(522, 233);
            this.var8Offset.Name = "var8Offset";
            this.var8Offset.Size = new System.Drawing.Size(63, 20);
            this.var8Offset.TabIndex = 437;
            this.var8Offset.TextChanged += new System.EventHandler(this.textBox35_TextChanged);
            // 
            // var7Offset
            // 
            this.var7Offset.Location = new System.Drawing.Point(522, 206);
            this.var7Offset.Name = "var7Offset";
            this.var7Offset.Size = new System.Drawing.Size(63, 20);
            this.var7Offset.TabIndex = 436;
            // 
            // var12OutputMax
            // 
            this.var12OutputMax.Location = new System.Drawing.Point(441, 335);
            this.var12OutputMax.Name = "var12OutputMax";
            this.var12OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var12OutputMax.TabIndex = 435;
            // 
            // var12OutputMin
            // 
            this.var12OutputMin.Location = new System.Drawing.Point(364, 335);
            this.var12OutputMin.Name = "var12OutputMin";
            this.var12OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var12OutputMin.TabIndex = 434;
            // 
            // var12InputMax
            // 
            this.var12InputMax.Location = new System.Drawing.Point(286, 335);
            this.var12InputMax.Name = "var12InputMax";
            this.var12InputMax.Size = new System.Drawing.Size(63, 20);
            this.var12InputMax.TabIndex = 433;
            // 
            // var12InputMin
            // 
            this.var12InputMin.Location = new System.Drawing.Point(210, 334);
            this.var12InputMin.Name = "var12InputMin";
            this.var12InputMin.Size = new System.Drawing.Size(63, 20);
            this.var12InputMin.TabIndex = 432;
            // 
            // var12Channel
            // 
            this.var12Channel.Location = new System.Drawing.Point(129, 335);
            this.var12Channel.Name = "var12Channel";
            this.var12Channel.Size = new System.Drawing.Size(63, 20);
            this.var12Channel.TabIndex = 430;
            // 
            // var11OutputMax
            // 
            this.var11OutputMax.Location = new System.Drawing.Point(441, 308);
            this.var11OutputMax.Name = "var11OutputMax";
            this.var11OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var11OutputMax.TabIndex = 429;
            // 
            // var11OutputMin
            // 
            this.var11OutputMin.Location = new System.Drawing.Point(364, 308);
            this.var11OutputMin.Name = "var11OutputMin";
            this.var11OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var11OutputMin.TabIndex = 428;
            // 
            // var11InputMax
            // 
            this.var11InputMax.Location = new System.Drawing.Point(286, 308);
            this.var11InputMax.Name = "var11InputMax";
            this.var11InputMax.Size = new System.Drawing.Size(63, 20);
            this.var11InputMax.TabIndex = 427;
            // 
            // var11InputMin
            // 
            this.var11InputMin.Location = new System.Drawing.Point(210, 307);
            this.var11InputMin.Name = "var11InputMin";
            this.var11InputMin.Size = new System.Drawing.Size(63, 20);
            this.var11InputMin.TabIndex = 426;
            // 
            // var11Channel
            // 
            this.var11Channel.Location = new System.Drawing.Point(129, 308);
            this.var11Channel.Name = "var11Channel";
            this.var11Channel.Size = new System.Drawing.Size(63, 20);
            this.var11Channel.TabIndex = 424;
            // 
            // var10OutputMax
            // 
            this.var10OutputMax.Location = new System.Drawing.Point(441, 283);
            this.var10OutputMax.Name = "var10OutputMax";
            this.var10OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var10OutputMax.TabIndex = 421;
            // 
            // var10OutputMin
            // 
            this.var10OutputMin.Location = new System.Drawing.Point(364, 283);
            this.var10OutputMin.Name = "var10OutputMin";
            this.var10OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var10OutputMin.TabIndex = 420;
            // 
            // var10InputMax
            // 
            this.var10InputMax.Location = new System.Drawing.Point(286, 283);
            this.var10InputMax.Name = "var10InputMax";
            this.var10InputMax.Size = new System.Drawing.Size(63, 20);
            this.var10InputMax.TabIndex = 419;
            // 
            // var10InputMin
            // 
            this.var10InputMin.Location = new System.Drawing.Point(210, 283);
            this.var10InputMin.Name = "var10InputMin";
            this.var10InputMin.Size = new System.Drawing.Size(63, 20);
            this.var10InputMin.TabIndex = 418;
            // 
            // var10Channel
            // 
            this.var10Channel.Location = new System.Drawing.Point(129, 283);
            this.var10Channel.Name = "var10Channel";
            this.var10Channel.Size = new System.Drawing.Size(63, 20);
            this.var10Channel.TabIndex = 416;
            // 
            // var9OutputMax
            // 
            this.var9OutputMax.Location = new System.Drawing.Point(441, 257);
            this.var9OutputMax.Name = "var9OutputMax";
            this.var9OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var9OutputMax.TabIndex = 415;
            // 
            // var9OutputMin
            // 
            this.var9OutputMin.Location = new System.Drawing.Point(364, 257);
            this.var9OutputMin.Name = "var9OutputMin";
            this.var9OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var9OutputMin.TabIndex = 414;
            // 
            // var9InputMax
            // 
            this.var9InputMax.Location = new System.Drawing.Point(286, 257);
            this.var9InputMax.Name = "var9InputMax";
            this.var9InputMax.Size = new System.Drawing.Size(63, 20);
            this.var9InputMax.TabIndex = 413;
            // 
            // var9InputMin
            // 
            this.var9InputMin.Location = new System.Drawing.Point(210, 256);
            this.var9InputMin.Name = "var9InputMin";
            this.var9InputMin.Size = new System.Drawing.Size(63, 20);
            this.var9InputMin.TabIndex = 412;
            // 
            // var9Channel
            // 
            this.var9Channel.Location = new System.Drawing.Point(129, 257);
            this.var9Channel.Name = "var9Channel";
            this.var9Channel.Size = new System.Drawing.Size(63, 20);
            this.var9Channel.TabIndex = 410;
            // 
            // var8OutputMax
            // 
            this.var8OutputMax.Location = new System.Drawing.Point(441, 232);
            this.var8OutputMax.Name = "var8OutputMax";
            this.var8OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var8OutputMax.TabIndex = 407;
            // 
            // var8OutputMin
            // 
            this.var8OutputMin.Location = new System.Drawing.Point(364, 232);
            this.var8OutputMin.Name = "var8OutputMin";
            this.var8OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var8OutputMin.TabIndex = 406;
            // 
            // var8InputMax
            // 
            this.var8InputMax.Location = new System.Drawing.Point(286, 232);
            this.var8InputMax.Name = "var8InputMax";
            this.var8InputMax.Size = new System.Drawing.Size(63, 20);
            this.var8InputMax.TabIndex = 405;
            // 
            // var8InputMin
            // 
            this.var8InputMin.Location = new System.Drawing.Point(210, 231);
            this.var8InputMin.Name = "var8InputMin";
            this.var8InputMin.Size = new System.Drawing.Size(63, 20);
            this.var8InputMin.TabIndex = 404;
            // 
            // var8Channel
            // 
            this.var8Channel.Location = new System.Drawing.Point(129, 232);
            this.var8Channel.Name = "var8Channel";
            this.var8Channel.Size = new System.Drawing.Size(63, 20);
            this.var8Channel.TabIndex = 402;
            // 
            // var7OutputMax
            // 
            this.var7OutputMax.Location = new System.Drawing.Point(441, 205);
            this.var7OutputMax.Name = "var7OutputMax";
            this.var7OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var7OutputMax.TabIndex = 401;
            // 
            // var7OutputMin
            // 
            this.var7OutputMin.Location = new System.Drawing.Point(364, 205);
            this.var7OutputMin.Name = "var7OutputMin";
            this.var7OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var7OutputMin.TabIndex = 400;
            // 
            // var7InputMax
            // 
            this.var7InputMax.Location = new System.Drawing.Point(286, 205);
            this.var7InputMax.Name = "var7InputMax";
            this.var7InputMax.Size = new System.Drawing.Size(63, 20);
            this.var7InputMax.TabIndex = 399;
            // 
            // var7InputMin
            // 
            this.var7InputMin.Location = new System.Drawing.Point(210, 205);
            this.var7InputMin.Name = "var7InputMin";
            this.var7InputMin.Size = new System.Drawing.Size(63, 20);
            this.var7InputMin.TabIndex = 398;
            // 
            // var7Channel
            // 
            this.var7Channel.Location = new System.Drawing.Point(129, 205);
            this.var7Channel.Name = "var7Channel";
            this.var7Channel.Size = new System.Drawing.Size(63, 20);
            this.var7Channel.TabIndex = 396;
            // 
            // var6DisplayName
            // 
            this.var6DisplayName.Location = new System.Drawing.Point(923, 179);
            this.var6DisplayName.Name = "var6DisplayName";
            this.var6DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var6DisplayName.TabIndex = 393;
            // 
            // var5DisplayName
            // 
            this.var5DisplayName.Location = new System.Drawing.Point(923, 153);
            this.var5DisplayName.Name = "var5DisplayName";
            this.var5DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var5DisplayName.TabIndex = 392;
            // 
            // var4DisplayName
            // 
            this.var4DisplayName.Location = new System.Drawing.Point(923, 128);
            this.var4DisplayName.Name = "var4DisplayName";
            this.var4DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var4DisplayName.TabIndex = 391;
            // 
            // var3DisplayName
            // 
            this.var3DisplayName.Location = new System.Drawing.Point(923, 101);
            this.var3DisplayName.Name = "var3DisplayName";
            this.var3DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var3DisplayName.TabIndex = 390;
            // 
            // var2DisplayName
            // 
            this.var2DisplayName.Location = new System.Drawing.Point(923, 77);
            this.var2DisplayName.Name = "var2DisplayName";
            this.var2DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var2DisplayName.TabIndex = 389;
            // 
            // var1DisplayName
            // 
            this.var1DisplayName.Location = new System.Drawing.Point(923, 50);
            this.var1DisplayName.Name = "var1DisplayName";
            this.var1DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var1DisplayName.TabIndex = 388;
            // 
            // var6Type
            // 
            this.var6Type.Location = new System.Drawing.Point(845, 179);
            this.var6Type.Name = "var6Type";
            this.var6Type.Size = new System.Drawing.Size(63, 20);
            this.var6Type.TabIndex = 386;
            // 
            // var5Type
            // 
            this.var5Type.Location = new System.Drawing.Point(845, 153);
            this.var5Type.Name = "var5Type";
            this.var5Type.Size = new System.Drawing.Size(63, 20);
            this.var5Type.TabIndex = 385;
            // 
            // var4Type
            // 
            this.var4Type.Location = new System.Drawing.Point(845, 128);
            this.var4Type.Name = "var4Type";
            this.var4Type.Size = new System.Drawing.Size(63, 20);
            this.var4Type.TabIndex = 384;
            // 
            // var3Type
            // 
            this.var3Type.Location = new System.Drawing.Point(845, 101);
            this.var3Type.Name = "var3Type";
            this.var3Type.Size = new System.Drawing.Size(63, 20);
            this.var3Type.TabIndex = 383;
            // 
            // var2Type
            // 
            this.var2Type.Location = new System.Drawing.Point(845, 77);
            this.var2Type.Name = "var2Type";
            this.var2Type.Size = new System.Drawing.Size(63, 20);
            this.var2Type.TabIndex = 382;
            // 
            // var1Type
            // 
            this.var1Type.Location = new System.Drawing.Point(845, 50);
            this.var1Type.Name = "var1Type";
            this.var1Type.Size = new System.Drawing.Size(63, 20);
            this.var1Type.TabIndex = 381;
            // 
            // var6StatusColumn
            // 
            this.var6StatusColumn.Enabled = false;
            this.var6StatusColumn.Location = new System.Drawing.Point(761, 179);
            this.var6StatusColumn.Name = "var6StatusColumn";
            this.var6StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var6StatusColumn.TabIndex = 379;
            // 
            // var5StatusColumn
            // 
            this.var5StatusColumn.Enabled = false;
            this.var5StatusColumn.Location = new System.Drawing.Point(761, 153);
            this.var5StatusColumn.Name = "var5StatusColumn";
            this.var5StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var5StatusColumn.TabIndex = 378;
            // 
            // var4StatusColumn
            // 
            this.var4StatusColumn.Enabled = false;
            this.var4StatusColumn.Location = new System.Drawing.Point(761, 128);
            this.var4StatusColumn.Name = "var4StatusColumn";
            this.var4StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var4StatusColumn.TabIndex = 377;
            // 
            // var3StatusColumn
            // 
            this.var3StatusColumn.Enabled = false;
            this.var3StatusColumn.Location = new System.Drawing.Point(761, 101);
            this.var3StatusColumn.Name = "var3StatusColumn";
            this.var3StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var3StatusColumn.TabIndex = 376;
            // 
            // var2StatusColumn
            // 
            this.var2StatusColumn.Enabled = false;
            this.var2StatusColumn.Location = new System.Drawing.Point(761, 77);
            this.var2StatusColumn.Name = "var2StatusColumn";
            this.var2StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var2StatusColumn.TabIndex = 375;
            // 
            // var1StatusColumn
            // 
            this.var1StatusColumn.Enabled = false;
            this.var1StatusColumn.Location = new System.Drawing.Point(761, 50);
            this.var1StatusColumn.Name = "var1StatusColumn";
            this.var1StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var1StatusColumn.TabIndex = 374;
            // 
            // var6ValueColumn
            // 
            this.var6ValueColumn.Enabled = false;
            this.var6ValueColumn.Location = new System.Drawing.Point(680, 179);
            this.var6ValueColumn.Name = "var6ValueColumn";
            this.var6ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var6ValueColumn.TabIndex = 372;
            // 
            // var6Unit
            // 
            this.var6Unit.Location = new System.Drawing.Point(603, 179);
            this.var6Unit.Name = "var6Unit";
            this.var6Unit.Size = new System.Drawing.Size(63, 20);
            this.var6Unit.TabIndex = 371;
            // 
            // var5ValueColumn
            // 
            this.var5ValueColumn.Enabled = false;
            this.var5ValueColumn.Location = new System.Drawing.Point(680, 152);
            this.var5ValueColumn.Name = "var5ValueColumn";
            this.var5ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var5ValueColumn.TabIndex = 370;
            // 
            // var5Unit
            // 
            this.var5Unit.Location = new System.Drawing.Point(603, 152);
            this.var5Unit.Name = "var5Unit";
            this.var5Unit.Size = new System.Drawing.Size(63, 20);
            this.var5Unit.TabIndex = 369;
            // 
            // var4ValueColumn
            // 
            this.var4ValueColumn.Enabled = false;
            this.var4ValueColumn.Location = new System.Drawing.Point(680, 127);
            this.var4ValueColumn.Name = "var4ValueColumn";
            this.var4ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var4ValueColumn.TabIndex = 368;
            // 
            // var4Unit
            // 
            this.var4Unit.Location = new System.Drawing.Point(603, 127);
            this.var4Unit.Name = "var4Unit";
            this.var4Unit.Size = new System.Drawing.Size(63, 20);
            this.var4Unit.TabIndex = 367;
            // 
            // var3ValueColumn
            // 
            this.var3ValueColumn.Enabled = false;
            this.var3ValueColumn.Location = new System.Drawing.Point(680, 101);
            this.var3ValueColumn.Name = "var3ValueColumn";
            this.var3ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var3ValueColumn.TabIndex = 366;
            // 
            // var3Unit
            // 
            this.var3Unit.Location = new System.Drawing.Point(603, 101);
            this.var3Unit.Name = "var3Unit";
            this.var3Unit.Size = new System.Drawing.Size(63, 20);
            this.var3Unit.TabIndex = 365;
            // 
            // var2ValueColumn
            // 
            this.var2ValueColumn.Enabled = false;
            this.var2ValueColumn.Location = new System.Drawing.Point(680, 76);
            this.var2ValueColumn.Name = "var2ValueColumn";
            this.var2ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var2ValueColumn.TabIndex = 364;
            // 
            // var2Unit
            // 
            this.var2Unit.Location = new System.Drawing.Point(603, 76);
            this.var2Unit.Name = "var2Unit";
            this.var2Unit.Size = new System.Drawing.Size(63, 20);
            this.var2Unit.TabIndex = 363;
            // 
            // var1ValueColumn
            // 
            this.var1ValueColumn.Enabled = false;
            this.var1ValueColumn.Location = new System.Drawing.Point(680, 49);
            this.var1ValueColumn.Name = "var1ValueColumn";
            this.var1ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var1ValueColumn.TabIndex = 362;
            // 
            // var1Unit
            // 
            this.var1Unit.Location = new System.Drawing.Point(603, 49);
            this.var1Unit.Name = "var1Unit";
            this.var1Unit.Size = new System.Drawing.Size(63, 20);
            this.var1Unit.TabIndex = 361;
            // 
            // var6Offset
            // 
            this.var6Offset.Location = new System.Drawing.Point(522, 179);
            this.var6Offset.Name = "var6Offset";
            this.var6Offset.Size = new System.Drawing.Size(63, 20);
            this.var6Offset.TabIndex = 358;
            // 
            // var5Offset
            // 
            this.var5Offset.Location = new System.Drawing.Point(522, 153);
            this.var5Offset.Name = "var5Offset";
            this.var5Offset.Size = new System.Drawing.Size(63, 20);
            this.var5Offset.TabIndex = 357;
            // 
            // var4Offset
            // 
            this.var4Offset.Location = new System.Drawing.Point(522, 128);
            this.var4Offset.Name = "var4Offset";
            this.var4Offset.Size = new System.Drawing.Size(63, 20);
            this.var4Offset.TabIndex = 356;
            // 
            // var3Offset
            // 
            this.var3Offset.Location = new System.Drawing.Point(522, 101);
            this.var3Offset.Name = "var3Offset";
            this.var3Offset.Size = new System.Drawing.Size(63, 20);
            this.var3Offset.TabIndex = 355;
            // 
            // var2Offset
            // 
            this.var2Offset.Location = new System.Drawing.Point(522, 77);
            this.var2Offset.Name = "var2Offset";
            this.var2Offset.Size = new System.Drawing.Size(63, 20);
            this.var2Offset.TabIndex = 354;
            // 
            // var1Offset
            // 
            this.var1Offset.Location = new System.Drawing.Point(522, 50);
            this.var1Offset.Name = "var1Offset";
            this.var1Offset.Size = new System.Drawing.Size(63, 20);
            this.var1Offset.TabIndex = 353;
            // 
            // var6OutputMax
            // 
            this.var6OutputMax.Location = new System.Drawing.Point(441, 179);
            this.var6OutputMax.Name = "var6OutputMax";
            this.var6OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var6OutputMax.TabIndex = 349;
            // 
            // var6OutputMin
            // 
            this.var6OutputMin.Location = new System.Drawing.Point(364, 179);
            this.var6OutputMin.Name = "var6OutputMin";
            this.var6OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var6OutputMin.TabIndex = 348;
            // 
            // var6InputMax
            // 
            this.var6InputMax.Location = new System.Drawing.Point(286, 179);
            this.var6InputMax.Name = "var6InputMax";
            this.var6InputMax.Size = new System.Drawing.Size(63, 20);
            this.var6InputMax.TabIndex = 347;
            // 
            // var6InputMin
            // 
            this.var6InputMin.Location = new System.Drawing.Point(210, 178);
            this.var6InputMin.Name = "var6InputMin";
            this.var6InputMin.Size = new System.Drawing.Size(63, 20);
            this.var6InputMin.TabIndex = 346;
            // 
            // var6Channel
            // 
            this.var6Channel.Location = new System.Drawing.Point(129, 179);
            this.var6Channel.Name = "var6Channel";
            this.var6Channel.Size = new System.Drawing.Size(63, 20);
            this.var6Channel.TabIndex = 344;
            // 
            // var5OutputMax
            // 
            this.var5OutputMax.Location = new System.Drawing.Point(441, 152);
            this.var5OutputMax.Name = "var5OutputMax";
            this.var5OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var5OutputMax.TabIndex = 343;
            // 
            // var5OutputMin
            // 
            this.var5OutputMin.Location = new System.Drawing.Point(364, 152);
            this.var5OutputMin.Name = "var5OutputMin";
            this.var5OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var5OutputMin.TabIndex = 342;
            // 
            // var5InputMax
            // 
            this.var5InputMax.Location = new System.Drawing.Point(286, 152);
            this.var5InputMax.Name = "var5InputMax";
            this.var5InputMax.Size = new System.Drawing.Size(63, 20);
            this.var5InputMax.TabIndex = 341;
            // 
            // var5InputMin
            // 
            this.var5InputMin.Location = new System.Drawing.Point(210, 151);
            this.var5InputMin.Name = "var5InputMin";
            this.var5InputMin.Size = new System.Drawing.Size(63, 20);
            this.var5InputMin.TabIndex = 340;
            // 
            // var5Channel
            // 
            this.var5Channel.Location = new System.Drawing.Point(129, 152);
            this.var5Channel.Name = "var5Channel";
            this.var5Channel.Size = new System.Drawing.Size(63, 20);
            this.var5Channel.TabIndex = 338;
            // 
            // var4OutputMax
            // 
            this.var4OutputMax.Location = new System.Drawing.Point(441, 127);
            this.var4OutputMax.Name = "var4OutputMax";
            this.var4OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var4OutputMax.TabIndex = 335;
            // 
            // var4OutputMin
            // 
            this.var4OutputMin.Location = new System.Drawing.Point(364, 127);
            this.var4OutputMin.Name = "var4OutputMin";
            this.var4OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var4OutputMin.TabIndex = 334;
            // 
            // var4InputMax
            // 
            this.var4InputMax.Location = new System.Drawing.Point(286, 127);
            this.var4InputMax.Name = "var4InputMax";
            this.var4InputMax.Size = new System.Drawing.Size(63, 20);
            this.var4InputMax.TabIndex = 333;
            // 
            // var4InputMin
            // 
            this.var4InputMin.Location = new System.Drawing.Point(210, 127);
            this.var4InputMin.Name = "var4InputMin";
            this.var4InputMin.Size = new System.Drawing.Size(63, 20);
            this.var4InputMin.TabIndex = 332;
            // 
            // var4Channel
            // 
            this.var4Channel.Location = new System.Drawing.Point(129, 127);
            this.var4Channel.Name = "var4Channel";
            this.var4Channel.Size = new System.Drawing.Size(63, 20);
            this.var4Channel.TabIndex = 330;
            // 
            // var3OutputMax
            // 
            this.var3OutputMax.Location = new System.Drawing.Point(441, 101);
            this.var3OutputMax.Name = "var3OutputMax";
            this.var3OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var3OutputMax.TabIndex = 329;
            // 
            // var3OutputMin
            // 
            this.var3OutputMin.Location = new System.Drawing.Point(364, 101);
            this.var3OutputMin.Name = "var3OutputMin";
            this.var3OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var3OutputMin.TabIndex = 328;
            // 
            // var3InputMax
            // 
            this.var3InputMax.Location = new System.Drawing.Point(286, 101);
            this.var3InputMax.Name = "var3InputMax";
            this.var3InputMax.Size = new System.Drawing.Size(63, 20);
            this.var3InputMax.TabIndex = 327;
            // 
            // var3InputMin
            // 
            this.var3InputMin.Location = new System.Drawing.Point(210, 100);
            this.var3InputMin.Name = "var3InputMin";
            this.var3InputMin.Size = new System.Drawing.Size(63, 20);
            this.var3InputMin.TabIndex = 326;
            // 
            // var3Channel
            // 
            this.var3Channel.Location = new System.Drawing.Point(129, 101);
            this.var3Channel.Name = "var3Channel";
            this.var3Channel.Size = new System.Drawing.Size(63, 20);
            this.var3Channel.TabIndex = 324;
            // 
            // var2OutputMax
            // 
            this.var2OutputMax.Location = new System.Drawing.Point(441, 76);
            this.var2OutputMax.Name = "var2OutputMax";
            this.var2OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var2OutputMax.TabIndex = 321;
            // 
            // var2OutputMin
            // 
            this.var2OutputMin.Location = new System.Drawing.Point(364, 76);
            this.var2OutputMin.Name = "var2OutputMin";
            this.var2OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var2OutputMin.TabIndex = 320;
            // 
            // var2InputMax
            // 
            this.var2InputMax.Location = new System.Drawing.Point(286, 76);
            this.var2InputMax.Name = "var2InputMax";
            this.var2InputMax.Size = new System.Drawing.Size(63, 20);
            this.var2InputMax.TabIndex = 319;
            // 
            // var2InputMin
            // 
            this.var2InputMin.Location = new System.Drawing.Point(210, 75);
            this.var2InputMin.Name = "var2InputMin";
            this.var2InputMin.Size = new System.Drawing.Size(63, 20);
            this.var2InputMin.TabIndex = 318;
            // 
            // var2Channel
            // 
            this.var2Channel.Location = new System.Drawing.Point(129, 76);
            this.var2Channel.Name = "var2Channel";
            this.var2Channel.Size = new System.Drawing.Size(63, 20);
            this.var2Channel.TabIndex = 316;
            // 
            // var1OutputMax
            // 
            this.var1OutputMax.Location = new System.Drawing.Point(441, 49);
            this.var1OutputMax.Name = "var1OutputMax";
            this.var1OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var1OutputMax.TabIndex = 315;
            // 
            // var1OutputMin
            // 
            this.var1OutputMin.Location = new System.Drawing.Point(364, 49);
            this.var1OutputMin.Name = "var1OutputMin";
            this.var1OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var1OutputMin.TabIndex = 314;
            // 
            // var1InputMax
            // 
            this.var1InputMax.Location = new System.Drawing.Point(286, 49);
            this.var1InputMax.Name = "var1InputMax";
            this.var1InputMax.Size = new System.Drawing.Size(63, 20);
            this.var1InputMax.TabIndex = 313;
            // 
            // var1InputMin
            // 
            this.var1InputMin.Location = new System.Drawing.Point(210, 49);
            this.var1InputMin.Name = "var1InputMin";
            this.var1InputMin.Size = new System.Drawing.Size(63, 20);
            this.var1InputMin.TabIndex = 312;
            // 
            // var1Channel
            // 
            this.var1Channel.Location = new System.Drawing.Point(129, 49);
            this.var1Channel.Name = "var1Channel";
            this.var1Channel.Size = new System.Drawing.Size(63, 20);
            this.var1Channel.TabIndex = 310;
            // 
            // var18Module
            // 
            this.var18Module.FormattingEnabled = true;
            this.var18Module.Location = new System.Drawing.Point(57, 490);
            this.var18Module.Name = "var18Module";
            this.var18Module.Size = new System.Drawing.Size(53, 21);
            this.var18Module.TabIndex = 509;
            this.var18Module.Visible = false;
            // 
            // var17Module
            // 
            this.var17Module.FormattingEnabled = true;
            this.var17Module.Location = new System.Drawing.Point(57, 463);
            this.var17Module.Name = "var17Module";
            this.var17Module.Size = new System.Drawing.Size(53, 21);
            this.var17Module.TabIndex = 503;
            this.var17Module.Visible = false;
            // 
            // var18
            // 
            this.var18.AutoSize = true;
            this.var18.Location = new System.Drawing.Point(15, 493);
            this.var18.Name = "var18";
            this.var18.Size = new System.Drawing.Size(34, 13);
            this.var18.TabIndex = 501;
            this.var18.Text = "var18";
            this.var18.Visible = false;
            // 
            // var17
            // 
            this.var17.AutoSize = true;
            this.var17.Location = new System.Drawing.Point(15, 468);
            this.var17.Name = "var17";
            this.var17.Size = new System.Drawing.Size(34, 13);
            this.var17.TabIndex = 500;
            this.var17.Text = "var17";
            this.var17.Visible = false;
            // 
            // var16Module
            // 
            this.var16Module.FormattingEnabled = true;
            this.var16Module.Location = new System.Drawing.Point(57, 439);
            this.var16Module.Name = "var16Module";
            this.var16Module.Size = new System.Drawing.Size(53, 21);
            this.var16Module.TabIndex = 495;
            this.var16Module.Visible = false;
            // 
            // var15Module
            // 
            this.var15Module.FormattingEnabled = true;
            this.var15Module.Location = new System.Drawing.Point(57, 412);
            this.var15Module.Name = "var15Module";
            this.var15Module.Size = new System.Drawing.Size(53, 21);
            this.var15Module.TabIndex = 489;
            this.var15Module.Visible = false;
            // 
            // var16
            // 
            this.var16.AutoSize = true;
            this.var16.Location = new System.Drawing.Point(15, 443);
            this.var16.Name = "var16";
            this.var16.Size = new System.Drawing.Size(34, 13);
            this.var16.TabIndex = 487;
            this.var16.Text = "var16";
            this.var16.Visible = false;
            // 
            // var15
            // 
            this.var15.AutoSize = true;
            this.var15.Location = new System.Drawing.Point(15, 415);
            this.var15.Name = "var15";
            this.var15.Size = new System.Drawing.Size(34, 13);
            this.var15.TabIndex = 486;
            this.var15.Text = "var15";
            this.var15.Visible = false;
            // 
            // var14Module
            // 
            this.var14Module.FormattingEnabled = true;
            this.var14Module.Location = new System.Drawing.Point(57, 387);
            this.var14Module.Name = "var14Module";
            this.var14Module.Size = new System.Drawing.Size(53, 21);
            this.var14Module.TabIndex = 481;
            this.var14Module.Visible = false;
            // 
            // var13Module
            // 
            this.var13Module.FormattingEnabled = true;
            this.var13Module.Location = new System.Drawing.Point(57, 361);
            this.var13Module.Name = "var13Module";
            this.var13Module.Size = new System.Drawing.Size(53, 21);
            this.var13Module.TabIndex = 475;
            // 
            // var14
            // 
            this.var14.AutoSize = true;
            this.var14.Location = new System.Drawing.Point(15, 390);
            this.var14.Name = "var14";
            this.var14.Size = new System.Drawing.Size(34, 13);
            this.var14.TabIndex = 473;
            this.var14.Text = "var14";
            this.var14.Visible = false;
            // 
            // var13
            // 
            this.var13.AutoSize = true;
            this.var13.Location = new System.Drawing.Point(15, 365);
            this.var13.Name = "var13";
            this.var13.Size = new System.Drawing.Size(34, 13);
            this.var13.TabIndex = 472;
            this.var13.Text = "var13";
            // 
            // var12Module
            // 
            this.var12Module.FormattingEnabled = true;
            this.var12Module.Location = new System.Drawing.Point(57, 334);
            this.var12Module.Name = "var12Module";
            this.var12Module.Size = new System.Drawing.Size(53, 21);
            this.var12Module.TabIndex = 431;
            // 
            // var11Module
            // 
            this.var11Module.FormattingEnabled = true;
            this.var11Module.Location = new System.Drawing.Point(57, 307);
            this.var11Module.Name = "var11Module";
            this.var11Module.Size = new System.Drawing.Size(53, 21);
            this.var11Module.TabIndex = 425;
            // 
            // var12
            // 
            this.var12.AutoSize = true;
            this.var12.Location = new System.Drawing.Point(15, 337);
            this.var12.Name = "var12";
            this.var12.Size = new System.Drawing.Size(34, 13);
            this.var12.TabIndex = 423;
            this.var12.Text = "var12";
            // 
            // var11
            // 
            this.var11.AutoSize = true;
            this.var11.Location = new System.Drawing.Point(15, 312);
            this.var11.Name = "var11";
            this.var11.Size = new System.Drawing.Size(34, 13);
            this.var11.TabIndex = 422;
            this.var11.Text = "var11";
            // 
            // var10Module
            // 
            this.var10Module.FormattingEnabled = true;
            this.var10Module.Location = new System.Drawing.Point(57, 283);
            this.var10Module.Name = "var10Module";
            this.var10Module.Size = new System.Drawing.Size(53, 21);
            this.var10Module.TabIndex = 417;
            // 
            // var9Module
            // 
            this.var9Module.FormattingEnabled = true;
            this.var9Module.Location = new System.Drawing.Point(57, 256);
            this.var9Module.Name = "var9Module";
            this.var9Module.Size = new System.Drawing.Size(53, 21);
            this.var9Module.TabIndex = 411;
            // 
            // var10
            // 
            this.var10.AutoSize = true;
            this.var10.Location = new System.Drawing.Point(15, 287);
            this.var10.Name = "var10";
            this.var10.Size = new System.Drawing.Size(34, 13);
            this.var10.TabIndex = 409;
            this.var10.Text = "var10";
            // 
            // var9
            // 
            this.var9.AutoSize = true;
            this.var9.Location = new System.Drawing.Point(15, 259);
            this.var9.Name = "var9";
            this.var9.Size = new System.Drawing.Size(28, 13);
            this.var9.TabIndex = 408;
            this.var9.Text = "var9";
            // 
            // var8Module
            // 
            this.var8Module.FormattingEnabled = true;
            this.var8Module.Location = new System.Drawing.Point(57, 231);
            this.var8Module.Name = "var8Module";
            this.var8Module.Size = new System.Drawing.Size(53, 21);
            this.var8Module.TabIndex = 403;
            // 
            // var7Module
            // 
            this.var7Module.FormattingEnabled = true;
            this.var7Module.Location = new System.Drawing.Point(57, 205);
            this.var7Module.Name = "var7Module";
            this.var7Module.Size = new System.Drawing.Size(53, 21);
            this.var7Module.TabIndex = 397;
            // 
            // var8
            // 
            this.var8.AutoSize = true;
            this.var8.Location = new System.Drawing.Point(15, 234);
            this.var8.Name = "var8";
            this.var8.Size = new System.Drawing.Size(28, 13);
            this.var8.TabIndex = 395;
            this.var8.Text = "var8";
            // 
            // var7
            // 
            this.var7.AutoSize = true;
            this.var7.Location = new System.Drawing.Point(15, 209);
            this.var7.Name = "var7";
            this.var7.Size = new System.Drawing.Size(28, 13);
            this.var7.TabIndex = 394;
            this.var7.Text = "var7";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(919, 17);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(72, 13);
            this.label55.TabIndex = 387;
            this.label55.Text = "Display Name";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(841, 17);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(31, 13);
            this.label54.TabIndex = 380;
            this.label54.Text = "Type";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(757, 17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(75, 13);
            this.label52.TabIndex = 373;
            this.label52.Text = "Status Column";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(677, 16);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(72, 13);
            this.label53.TabIndex = 360;
            this.label53.Text = "Value Column";
            this.label53.Click += new System.EventHandler(this.label53_Click);
            // 
            // Unit
            // 
            this.Unit.AutoSize = true;
            this.Unit.Location = new System.Drawing.Point(605, 16);
            this.Unit.Name = "Unit";
            this.Unit.Size = new System.Drawing.Size(26, 13);
            this.Unit.TabIndex = 359;
            this.Unit.Text = "Unit";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(518, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 13);
            this.label28.TabIndex = 351;
            this.label28.Text = "Offset";
            // 
            // var6Module
            // 
            this.var6Module.FormattingEnabled = true;
            this.var6Module.Location = new System.Drawing.Point(57, 178);
            this.var6Module.Name = "var6Module";
            this.var6Module.Size = new System.Drawing.Size(53, 21);
            this.var6Module.TabIndex = 345;
            // 
            // var5Module
            // 
            this.var5Module.FormattingEnabled = true;
            this.var5Module.Location = new System.Drawing.Point(57, 151);
            this.var5Module.Name = "var5Module";
            this.var5Module.Size = new System.Drawing.Size(53, 21);
            this.var5Module.TabIndex = 339;
            // 
            // var6
            // 
            this.var6.AutoSize = true;
            this.var6.Location = new System.Drawing.Point(15, 181);
            this.var6.Name = "var6";
            this.var6.Size = new System.Drawing.Size(28, 13);
            this.var6.TabIndex = 337;
            this.var6.Text = "var6";
            // 
            // var5
            // 
            this.var5.AutoSize = true;
            this.var5.Location = new System.Drawing.Point(15, 156);
            this.var5.Name = "var5";
            this.var5.Size = new System.Drawing.Size(28, 13);
            this.var5.TabIndex = 336;
            this.var5.Text = "var5";
            // 
            // var4Module
            // 
            this.var4Module.FormattingEnabled = true;
            this.var4Module.Location = new System.Drawing.Point(57, 127);
            this.var4Module.Name = "var4Module";
            this.var4Module.Size = new System.Drawing.Size(53, 21);
            this.var4Module.TabIndex = 331;
            // 
            // var3Module
            // 
            this.var3Module.FormattingEnabled = true;
            this.var3Module.Location = new System.Drawing.Point(57, 100);
            this.var3Module.Name = "var3Module";
            this.var3Module.Size = new System.Drawing.Size(53, 21);
            this.var3Module.TabIndex = 325;
            // 
            // var4
            // 
            this.var4.AutoSize = true;
            this.var4.Location = new System.Drawing.Point(15, 131);
            this.var4.Name = "var4";
            this.var4.Size = new System.Drawing.Size(28, 13);
            this.var4.TabIndex = 323;
            this.var4.Text = "var4";
            this.var4.Click += new System.EventHandler(this.label24_Click);
            // 
            // var3
            // 
            this.var3.AutoSize = true;
            this.var3.Location = new System.Drawing.Point(15, 103);
            this.var3.Name = "var3";
            this.var3.Size = new System.Drawing.Size(28, 13);
            this.var3.TabIndex = 322;
            this.var3.Text = "var3";
            // 
            // var2Module
            // 
            this.var2Module.FormattingEnabled = true;
            this.var2Module.Location = new System.Drawing.Point(57, 75);
            this.var2Module.Name = "var2Module";
            this.var2Module.Size = new System.Drawing.Size(53, 21);
            this.var2Module.TabIndex = 317;
            // 
            // var1Module
            // 
            this.var1Module.FormattingEnabled = true;
            this.var1Module.Location = new System.Drawing.Point(57, 49);
            this.var1Module.Name = "var1Module";
            this.var1Module.Size = new System.Drawing.Size(53, 21);
            this.var1Module.TabIndex = 311;
            // 
            // var2
            // 
            this.var2.AutoSize = true;
            this.var2.Location = new System.Drawing.Point(15, 78);
            this.var2.Name = "var2";
            this.var2.Size = new System.Drawing.Size(28, 13);
            this.var2.TabIndex = 309;
            this.var2.Text = "var2";
            // 
            // var1
            // 
            this.var1.AutoSize = true;
            this.var1.Location = new System.Drawing.Point(15, 53);
            this.var1.Name = "var1";
            this.var1.Size = new System.Drawing.Size(28, 13);
            this.var1.TabIndex = 308;
            this.var1.Text = "var1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(438, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 301;
            this.label8.Text = "Output max";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(366, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 300;
            this.label9.Text = "Output min";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(284, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 297;
            this.label7.Text = "Input max";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(212, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 296;
            this.label6.Text = "Input min";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(125, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 295;
            this.label5.Text = "Channel No.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 294;
            this.label4.Text = "Module ID";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnShow);
            this.tabPage1.Controls.Add(this.btnSOCKET);
            this.tabPage1.Controls.Add(this.txtSocketPort);
            this.tabPage1.Controls.Add(this.txtStationID);
            this.tabPage1.Controls.Add(this.txtStationName);
            this.tabPage1.Controls.Add(this.lblSocketPort);
            this.tabPage1.Controls.Add(this.lblStationID);
            this.tabPage1.Controls.Add(this.lblStationName);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(866, 331);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Station";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(363, 113);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 23);
            this.btnShow.TabIndex = 20;
            this.btnShow.Text = "Hide";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Visible = false;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnSOCKET
            // 
            this.btnSOCKET.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSOCKET.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSOCKET.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSOCKET.ForeColor = System.Drawing.SystemColors.Window;
            this.btnSOCKET.Image = global::DataLogger.Properties.Resources.ON_switch_96x25;
            this.btnSOCKET.Location = new System.Drawing.Point(245, 102);
            this.btnSOCKET.Name = "btnSOCKET";
            this.btnSOCKET.Size = new System.Drawing.Size(81, 40);
            this.btnSOCKET.TabIndex = 19;
            this.btnSOCKET.UseVisualStyleBackColor = true;
            this.btnSOCKET.Click += new System.EventHandler(this.btnSOCKET_Click);
            // 
            // txtSocketPort
            // 
            this.txtSocketPort.Location = new System.Drawing.Point(88, 113);
            this.txtSocketPort.Name = "txtSocketPort";
            this.txtSocketPort.Size = new System.Drawing.Size(118, 20);
            this.txtSocketPort.TabIndex = 18;
            // 
            // txtStationID
            // 
            this.txtStationID.Location = new System.Drawing.Point(88, 63);
            this.txtStationID.Name = "txtStationID";
            this.txtStationID.Size = new System.Drawing.Size(238, 20);
            this.txtStationID.TabIndex = 17;
            // 
            // txtStationName
            // 
            this.txtStationName.Location = new System.Drawing.Point(88, 21);
            this.txtStationName.Name = "txtStationName";
            this.txtStationName.Size = new System.Drawing.Size(118, 20);
            this.txtStationName.TabIndex = 16;
            // 
            // lblSocketPort
            // 
            this.lblSocketPort.AutoSize = true;
            this.lblSocketPort.Location = new System.Drawing.Point(13, 116);
            this.lblSocketPort.Name = "lblSocketPort";
            this.lblSocketPort.Size = new System.Drawing.Size(62, 13);
            this.lblSocketPort.TabIndex = 15;
            this.lblSocketPort.Text = "Socket port";
            // 
            // lblStationID
            // 
            this.lblStationID.AutoSize = true;
            this.lblStationID.Location = new System.Drawing.Point(13, 66);
            this.lblStationID.Name = "lblStationID";
            this.lblStationID.Size = new System.Drawing.Size(54, 13);
            this.lblStationID.TabIndex = 14;
            this.lblStationID.Text = "Station ID";
            // 
            // lblStationName
            // 
            this.lblStationName.AutoSize = true;
            this.lblStationName.Location = new System.Drawing.Point(13, 24);
            this.lblStationName.Name = "lblStationName";
            this.lblStationName.Size = new System.Drawing.Size(69, 13);
            this.lblStationName.TabIndex = 13;
            this.lblStationName.Text = "Station name";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(9, 10);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(874, 357);
            this.tabControl1.TabIndex = 88;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(866, 331);
            this.tabPage2.TabIndex = 5;
            this.tabPage2.Text = "Push Config";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::DataLogger.Properties.Resources.Control;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.dgvData);
            this.panel1.Location = new System.Drawing.Point(31, 149);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(787, 238);
            this.panel1.TabIndex = 8;
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToOrderColumns = true;
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvData.BackgroundColor = System.Drawing.Color.White;
            this.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(11, 14);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(758, 208);
            this.dgvData.TabIndex = 2;
            this.dgvData.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbFlag);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textPwd);
            this.groupBox1.Controls.Add(this.label57);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtFolder);
            this.groupBox1.Controls.Add(this.label56);
            this.groupBox1.Controls.Add(this.dtpLastedValue);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtUsername);
            this.groupBox1.Controls.Add(this.txtIP);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(53)))), ((int)(((byte)(56)))));
            this.groupBox1.Location = new System.Drawing.Point(32, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(786, 108);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FTP Info";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(83)))), ((int)(((byte)(98)))));
            this.button2.Location = new System.Drawing.Point(680, 74);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(42, 23);
            this.button2.TabIndex = 61;
            this.button2.Text = "Add";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnDelete.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(83)))), ((int)(((byte)(98)))));
            this.btnDelete.Location = new System.Drawing.Point(616, 74);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(58, 23);
            this.btnDelete.TabIndex = 60;
            this.btnDelete.Text = "Delete selected User";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(83)))), ((int)(((byte)(98)))));
            this.button1.Location = new System.Drawing.Point(568, 74);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(42, 23);
            this.button1.TabIndex = 59;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textPwd
            // 
            this.textPwd.Location = new System.Drawing.Point(426, 46);
            this.textPwd.Name = "textPwd";
            this.textPwd.Size = new System.Drawing.Size(121, 21);
            this.textPwd.TabIndex = 58;
            // 
            // label57
            // 
            this.label57.Location = new System.Drawing.Point(349, 46);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(57, 20);
            this.label57.TabIndex = 57;
            this.label57.Text = "Password";
            this.label57.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(349, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Folder";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(426, 22);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(236, 21);
            this.txtFolder.TabIndex = 5;
            // 
            // label56
            // 
            this.label56.Location = new System.Drawing.Point(349, 74);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(68, 20);
            this.label56.TabIndex = 56;
            this.label56.Text = "Lasted Value";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpLastedValue
            // 
            this.dtpLastedValue.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpLastedValue.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLastedValue.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLastedValue.Location = new System.Drawing.Point(426, 72);
            this.dtpLastedValue.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.dtpLastedValue.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpLastedValue.Name = "dtpLastedValue";
            this.dtpLastedValue.Size = new System.Drawing.Size(121, 21);
            this.dtpLastedValue.TabIndex = 55;
            this.dtpLastedValue.Value = new System.DateTime(2015, 10, 22, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(52, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Flag";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(128, 46);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(121, 21);
            this.txtUsername.TabIndex = 6;
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(128, 21);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(121, 21);
            this.txtIP.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(52, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "Username";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(52, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 20);
            this.label11.TabIndex = 0;
            this.label11.Text = "IP";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbFlag
            // 
            this.cbFlag.Location = new System.Drawing.Point(128, 74);
            this.cbFlag.Name = "cbFlag";
            this.cbFlag.Size = new System.Drawing.Size(121, 21);
            this.cbFlag.TabIndex = 62;
            // 
            // frmConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 457);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.frmConfiguration_Load);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox cbModule;
        private System.Windows.Forms.Label lblModuleComport;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox var18DisplayName;
        private System.Windows.Forms.TextBox var17DisplayName;
        private System.Windows.Forms.TextBox var16DisplayName;
        private System.Windows.Forms.TextBox var15DisplayName;
        private System.Windows.Forms.TextBox var14DisplayName;
        private System.Windows.Forms.TextBox var13DisplayName;
        private System.Windows.Forms.TextBox var18Type;
        private System.Windows.Forms.TextBox var17Type;
        private System.Windows.Forms.TextBox var16Type;
        private System.Windows.Forms.TextBox var15Type;
        private System.Windows.Forms.TextBox var14Type;
        private System.Windows.Forms.TextBox var13Type;
        private System.Windows.Forms.TextBox var18StatusColumn;
        private System.Windows.Forms.TextBox var17StatusColumn;
        private System.Windows.Forms.TextBox var16StatusColumn;
        private System.Windows.Forms.TextBox var15StatusColumn;
        private System.Windows.Forms.TextBox var14StatusColumn;
        private System.Windows.Forms.TextBox var13StatusColumn;
        private System.Windows.Forms.TextBox var18ValueColumn;
        private System.Windows.Forms.TextBox var18Unit;
        private System.Windows.Forms.TextBox var17ValueColumn;
        private System.Windows.Forms.TextBox var17Unit;
        private System.Windows.Forms.TextBox var16ValueColumn;
        private System.Windows.Forms.TextBox var16Unit;
        private System.Windows.Forms.TextBox var15ValueColumn;
        private System.Windows.Forms.TextBox var15Unit;
        private System.Windows.Forms.TextBox var14ValueColumn;
        private System.Windows.Forms.TextBox var14Unit;
        private System.Windows.Forms.TextBox var13ValueColumn;
        private System.Windows.Forms.TextBox var13Unit;
        private System.Windows.Forms.TextBox var18Offset;
        private System.Windows.Forms.TextBox var17Offset;
        private System.Windows.Forms.TextBox var16Offset;
        private System.Windows.Forms.TextBox var15Offset;
        private System.Windows.Forms.TextBox var14Offset;
        private System.Windows.Forms.TextBox var13Offset;
        private System.Windows.Forms.TextBox var18OutputMax;
        private System.Windows.Forms.TextBox var18OutputMin;
        private System.Windows.Forms.TextBox var18InputMax;
        private System.Windows.Forms.TextBox var18InputMin;
        private System.Windows.Forms.TextBox var18Channel;
        private System.Windows.Forms.TextBox var17OutputMax;
        private System.Windows.Forms.TextBox var17OutputMin;
        private System.Windows.Forms.TextBox var17InputMax;
        private System.Windows.Forms.TextBox var17InputMin;
        private System.Windows.Forms.TextBox var17Channel;
        private System.Windows.Forms.TextBox var16OutputMax;
        private System.Windows.Forms.TextBox var16OutputMin;
        private System.Windows.Forms.TextBox var16InputMax;
        private System.Windows.Forms.TextBox var16InputMin;
        private System.Windows.Forms.TextBox var16Channel;
        private System.Windows.Forms.TextBox var15OutputMax;
        private System.Windows.Forms.TextBox var15OutputMin;
        private System.Windows.Forms.TextBox var15InputMax;
        private System.Windows.Forms.TextBox var15InputMin;
        private System.Windows.Forms.TextBox var15Channel;
        private System.Windows.Forms.TextBox var14OutputMax;
        private System.Windows.Forms.TextBox var14OutputMin;
        private System.Windows.Forms.TextBox var14InputMax;
        private System.Windows.Forms.TextBox var14InputMin;
        private System.Windows.Forms.TextBox var14Channel;
        private System.Windows.Forms.TextBox var13OutputMax;
        private System.Windows.Forms.TextBox var13OutputMin;
        private System.Windows.Forms.TextBox var13InputMax;
        private System.Windows.Forms.TextBox var13InputMin;
        private System.Windows.Forms.TextBox var13Channel;
        private System.Windows.Forms.TextBox var12DisplayName;
        private System.Windows.Forms.TextBox var11DisplayName;
        private System.Windows.Forms.TextBox var10DisplayName;
        private System.Windows.Forms.TextBox var9DisplayName;
        private System.Windows.Forms.TextBox var8DisplayName;
        private System.Windows.Forms.TextBox var7DisplayName;
        private System.Windows.Forms.TextBox var12Type;
        private System.Windows.Forms.TextBox var11Type;
        private System.Windows.Forms.TextBox var10Type;
        private System.Windows.Forms.TextBox var9Type;
        private System.Windows.Forms.TextBox var8Type;
        private System.Windows.Forms.TextBox var7Type;
        private System.Windows.Forms.TextBox var12StatusColumn;
        private System.Windows.Forms.TextBox var11StatusColumn;
        private System.Windows.Forms.TextBox var10StatusColumn;
        private System.Windows.Forms.TextBox var9StatusColumn;
        private System.Windows.Forms.TextBox var8StatusColumn;
        private System.Windows.Forms.TextBox var7StatusColumn;
        private System.Windows.Forms.TextBox var12ValueColumn;
        private System.Windows.Forms.TextBox var12Unit;
        private System.Windows.Forms.TextBox var11ValueColumn;
        private System.Windows.Forms.TextBox var11Unit;
        private System.Windows.Forms.TextBox var10ValueColumn;
        private System.Windows.Forms.TextBox var10Unit;
        private System.Windows.Forms.TextBox var9ValueColumn;
        private System.Windows.Forms.TextBox var9Unit;
        private System.Windows.Forms.TextBox var8ValueColumn;
        private System.Windows.Forms.TextBox var8Unit;
        private System.Windows.Forms.TextBox var7ValueColumn;
        private System.Windows.Forms.TextBox var7Unit;
        private System.Windows.Forms.TextBox var12Offset;
        private System.Windows.Forms.TextBox var11Offset;
        private System.Windows.Forms.TextBox var10Offset;
        private System.Windows.Forms.TextBox var9Offset;
        private System.Windows.Forms.TextBox var8Offset;
        private System.Windows.Forms.TextBox var7Offset;
        private System.Windows.Forms.TextBox var12OutputMax;
        private System.Windows.Forms.TextBox var12OutputMin;
        private System.Windows.Forms.TextBox var12InputMax;
        private System.Windows.Forms.TextBox var12InputMin;
        private System.Windows.Forms.TextBox var12Channel;
        private System.Windows.Forms.TextBox var11OutputMax;
        private System.Windows.Forms.TextBox var11OutputMin;
        private System.Windows.Forms.TextBox var11InputMax;
        private System.Windows.Forms.TextBox var11InputMin;
        private System.Windows.Forms.TextBox var11Channel;
        private System.Windows.Forms.TextBox var10OutputMax;
        private System.Windows.Forms.TextBox var10OutputMin;
        private System.Windows.Forms.TextBox var10InputMax;
        private System.Windows.Forms.TextBox var10InputMin;
        private System.Windows.Forms.TextBox var10Channel;
        private System.Windows.Forms.TextBox var9OutputMax;
        private System.Windows.Forms.TextBox var9OutputMin;
        private System.Windows.Forms.TextBox var9InputMax;
        private System.Windows.Forms.TextBox var9InputMin;
        private System.Windows.Forms.TextBox var9Channel;
        private System.Windows.Forms.TextBox var8OutputMax;
        private System.Windows.Forms.TextBox var8OutputMin;
        private System.Windows.Forms.TextBox var8InputMax;
        private System.Windows.Forms.TextBox var8InputMin;
        private System.Windows.Forms.TextBox var8Channel;
        private System.Windows.Forms.TextBox var7OutputMax;
        private System.Windows.Forms.TextBox var7OutputMin;
        private System.Windows.Forms.TextBox var7InputMax;
        private System.Windows.Forms.TextBox var7InputMin;
        private System.Windows.Forms.TextBox var7Channel;
        private System.Windows.Forms.TextBox var6DisplayName;
        private System.Windows.Forms.TextBox var5DisplayName;
        private System.Windows.Forms.TextBox var4DisplayName;
        private System.Windows.Forms.TextBox var3DisplayName;
        private System.Windows.Forms.TextBox var2DisplayName;
        private System.Windows.Forms.TextBox var1DisplayName;
        private System.Windows.Forms.TextBox var6Type;
        private System.Windows.Forms.TextBox var5Type;
        private System.Windows.Forms.TextBox var4Type;
        private System.Windows.Forms.TextBox var3Type;
        private System.Windows.Forms.TextBox var2Type;
        private System.Windows.Forms.TextBox var1Type;
        private System.Windows.Forms.TextBox var6StatusColumn;
        private System.Windows.Forms.TextBox var5StatusColumn;
        private System.Windows.Forms.TextBox var4StatusColumn;
        private System.Windows.Forms.TextBox var3StatusColumn;
        private System.Windows.Forms.TextBox var2StatusColumn;
        private System.Windows.Forms.TextBox var1StatusColumn;
        private System.Windows.Forms.TextBox var6ValueColumn;
        private System.Windows.Forms.TextBox var6Unit;
        private System.Windows.Forms.TextBox var5ValueColumn;
        private System.Windows.Forms.TextBox var5Unit;
        private System.Windows.Forms.TextBox var4ValueColumn;
        private System.Windows.Forms.TextBox var4Unit;
        private System.Windows.Forms.TextBox var3ValueColumn;
        private System.Windows.Forms.TextBox var3Unit;
        private System.Windows.Forms.TextBox var2ValueColumn;
        private System.Windows.Forms.TextBox var2Unit;
        private System.Windows.Forms.TextBox var1ValueColumn;
        private System.Windows.Forms.TextBox var1Unit;
        private System.Windows.Forms.TextBox var6Offset;
        private System.Windows.Forms.TextBox var5Offset;
        private System.Windows.Forms.TextBox var4Offset;
        private System.Windows.Forms.TextBox var3Offset;
        private System.Windows.Forms.TextBox var2Offset;
        private System.Windows.Forms.TextBox var1Offset;
        private System.Windows.Forms.TextBox var6OutputMax;
        private System.Windows.Forms.TextBox var6OutputMin;
        private System.Windows.Forms.TextBox var6InputMax;
        private System.Windows.Forms.TextBox var6InputMin;
        private System.Windows.Forms.TextBox var6Channel;
        private System.Windows.Forms.TextBox var5OutputMax;
        private System.Windows.Forms.TextBox var5OutputMin;
        private System.Windows.Forms.TextBox var5InputMax;
        private System.Windows.Forms.TextBox var5InputMin;
        private System.Windows.Forms.TextBox var5Channel;
        private System.Windows.Forms.TextBox var4OutputMax;
        private System.Windows.Forms.TextBox var4OutputMin;
        private System.Windows.Forms.TextBox var4InputMax;
        private System.Windows.Forms.TextBox var4InputMin;
        private System.Windows.Forms.TextBox var4Channel;
        private System.Windows.Forms.TextBox var3OutputMax;
        private System.Windows.Forms.TextBox var3OutputMin;
        private System.Windows.Forms.TextBox var3InputMax;
        private System.Windows.Forms.TextBox var3InputMin;
        private System.Windows.Forms.TextBox var3Channel;
        private System.Windows.Forms.TextBox var2OutputMax;
        private System.Windows.Forms.TextBox var2OutputMin;
        private System.Windows.Forms.TextBox var2InputMax;
        private System.Windows.Forms.TextBox var2InputMin;
        private System.Windows.Forms.TextBox var2Channel;
        private System.Windows.Forms.TextBox var1OutputMax;
        private System.Windows.Forms.TextBox var1OutputMin;
        private System.Windows.Forms.TextBox var1InputMax;
        private System.Windows.Forms.TextBox var1InputMin;
        private System.Windows.Forms.TextBox var1Channel;
        private System.Windows.Forms.ComboBox var18Module;
        private System.Windows.Forms.ComboBox var17Module;
        private System.Windows.Forms.Label var18;
        private System.Windows.Forms.Label var17;
        private System.Windows.Forms.ComboBox var16Module;
        private System.Windows.Forms.ComboBox var15Module;
        private System.Windows.Forms.Label var16;
        private System.Windows.Forms.Label var15;
        private System.Windows.Forms.ComboBox var14Module;
        private System.Windows.Forms.ComboBox var13Module;
        private System.Windows.Forms.Label var14;
        private System.Windows.Forms.Label var13;
        private System.Windows.Forms.ComboBox var12Module;
        private System.Windows.Forms.ComboBox var11Module;
        private System.Windows.Forms.Label var12;
        private System.Windows.Forms.Label var11;
        private System.Windows.Forms.ComboBox var10Module;
        private System.Windows.Forms.ComboBox var9Module;
        private System.Windows.Forms.Label var10;
        private System.Windows.Forms.Label var9;
        private System.Windows.Forms.ComboBox var8Module;
        private System.Windows.Forms.ComboBox var7Module;
        private System.Windows.Forms.Label var8;
        private System.Windows.Forms.Label var7;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label Unit;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox var6Module;
        private System.Windows.Forms.ComboBox var5Module;
        private System.Windows.Forms.Label var6;
        private System.Windows.Forms.Label var5;
        private System.Windows.Forms.ComboBox var4Module;
        private System.Windows.Forms.ComboBox var3Module;
        private System.Windows.Forms.Label var4;
        private System.Windows.Forms.Label var3;
        private System.Windows.Forms.ComboBox var2Module;
        private System.Windows.Forms.ComboBox var1Module;
        private System.Windows.Forms.Label var2;
        private System.Windows.Forms.Label var1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnShow;
        public System.Windows.Forms.Button btnSOCKET;
        private System.Windows.Forms.TextBox txtSocketPort;
        private System.Windows.Forms.TextBox txtStationID;
        private System.Windows.Forms.TextBox txtStationName;
        private System.Windows.Forms.Label lblSocketPort;
        private System.Windows.Forms.Label lblStationID;
        private System.Windows.Forms.Label lblStationName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TextBox var18ErrorMax;
        private System.Windows.Forms.TextBox var17ErrorMax;
        private System.Windows.Forms.TextBox var16ErrorMax;
        private System.Windows.Forms.TextBox var15ErrorMax;
        private System.Windows.Forms.TextBox var14ErrorMax;
        private System.Windows.Forms.TextBox var13ErrorMax;
        private System.Windows.Forms.TextBox var12ErrorMax;
        private System.Windows.Forms.TextBox var11ErrorMax;
        private System.Windows.Forms.TextBox var10ErrorMax;
        private System.Windows.Forms.TextBox var9ErrorMax;
        private System.Windows.Forms.TextBox var8ErrorMax;
        private System.Windows.Forms.TextBox var7ErrorMax;
        private System.Windows.Forms.TextBox var6ErrorMax;
        private System.Windows.Forms.TextBox var5ErrorMax;
        private System.Windows.Forms.TextBox var4ErrorMax;
        private System.Windows.Forms.TextBox var3ErrorMax;
        private System.Windows.Forms.TextBox var2ErrorMax;
        private System.Windows.Forms.TextBox var1ErrorMax;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textPwd;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.DateTimePicker dtpLastedValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox cbFlag;
    }
}