﻿namespace DataLogger
{
    partial class frm1HourMPS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblHeaderTitle = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtvar1 = new System.Windows.Forms.Label();
            this.txtvar3Value = new System.Windows.Forms.TextBox();
            this.txtvar2Value = new System.Windows.Forms.TextBox();
            this.txtvar4Value = new System.Windows.Forms.TextBox();
            this.txtvar1Value = new System.Windows.Forms.TextBox();
            this.txtvar5Value = new System.Windows.Forms.TextBox();
            this.txtvar6 = new System.Windows.Forms.Label();
            this.txtvar6Value = new System.Windows.Forms.TextBox();
            this.txtvar4 = new System.Windows.Forms.Label();
            this.txtvar3 = new System.Windows.Forms.Label();
            this.txtvar2 = new System.Windows.Forms.Label();
            this.txtvar5 = new System.Windows.Forms.Label();
            this.txtvar4Unit = new System.Windows.Forms.Label();
            this.txtvar5Unit = new System.Windows.Forms.Label();
            this.txtvar6Unit = new System.Windows.Forms.Label();
            this.txtvar2Unit = new System.Windows.Forms.Label();
            this.txtvar3Unit = new System.Windows.Forms.Label();
            this.txtvar1Unit = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtvar9Value = new System.Windows.Forms.TextBox();
            this.txtvar8Value = new System.Windows.Forms.TextBox();
            this.txtvar7Unit = new System.Windows.Forms.Label();
            this.txtvar10Value = new System.Windows.Forms.TextBox();
            this.txtvar9Unit = new System.Windows.Forms.Label();
            this.txtvar7Value = new System.Windows.Forms.TextBox();
            this.txtvar8Unit = new System.Windows.Forms.Label();
            this.txtvar11Value = new System.Windows.Forms.TextBox();
            this.txtvar12Unit = new System.Windows.Forms.Label();
            this.txtvar12 = new System.Windows.Forms.Label();
            this.txtvar11Unit = new System.Windows.Forms.Label();
            this.txtvar12Value = new System.Windows.Forms.TextBox();
            this.txtvar10Unit = new System.Windows.Forms.Label();
            this.txtvar10 = new System.Windows.Forms.Label();
            this.txtvar11 = new System.Windows.Forms.Label();
            this.txtvar9 = new System.Windows.Forms.Label();
            this.txtvar7 = new System.Windows.Forms.Label();
            this.txtvar8 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtvar13Unit = new System.Windows.Forms.Label();
            this.txtvar15Unit = new System.Windows.Forms.Label();
            this.txtvar14Unit = new System.Windows.Forms.Label();
            this.txtvar18Unit = new System.Windows.Forms.Label();
            this.txtvar17Unit = new System.Windows.Forms.Label();
            this.txtvar16Unit = new System.Windows.Forms.Label();
            this.txtvar17 = new System.Windows.Forms.Label();
            this.txtvar13 = new System.Windows.Forms.Label();
            this.txtvar14 = new System.Windows.Forms.Label();
            this.txtvar15 = new System.Windows.Forms.Label();
            this.txtvar16 = new System.Windows.Forms.Label();
            this.txtvar18Value = new System.Windows.Forms.TextBox();
            this.txtvar18 = new System.Windows.Forms.Label();
            this.txtvar17Value = new System.Windows.Forms.TextBox();
            this.txtvar13Value = new System.Windows.Forms.TextBox();
            this.txtvar16Value = new System.Windows.Forms.TextBox();
            this.txtvar14Value = new System.Windows.Forms.TextBox();
            this.txtvar15Value = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(100)))), ((int)(((byte)(98)))));
            this.label1.Location = new System.Drawing.Point(39, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 45);
            this.label1.TabIndex = 70;
            this.label1.Text = "MPS";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(71)))), ((int)(((byte)(117)))));
            this.panel1.Controls.Add(this.lblHeaderTitle);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Location = new System.Drawing.Point(17, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(140, 40);
            this.panel1.TabIndex = 86;
            // 
            // lblHeaderTitle
            // 
            this.lblHeaderTitle.AutoSize = true;
            this.lblHeaderTitle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderTitle.ForeColor = System.Drawing.Color.White;
            this.lblHeaderTitle.Location = new System.Drawing.Point(13, 10);
            this.lblHeaderTitle.Name = "lblHeaderTitle";
            this.lblHeaderTitle.Size = new System.Drawing.Size(107, 19);
            this.lblHeaderTitle.TabIndex = 87;
            this.lblHeaderTitle.Text = "1 Hour Data";
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExit.BackgroundImage = global::DataLogger.Properties.Resources.Shutdown_Box_Red;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Enabled = false;
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Location = new System.Drawing.Point(184, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(40, 43);
            this.btnExit.TabIndex = 87;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Visible = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(167, 41);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(462, 277);
            this.tabControl1.TabIndex = 106;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtvar1);
            this.tabPage2.Controls.Add(this.txtvar3Value);
            this.tabPage2.Controls.Add(this.txtvar2Value);
            this.tabPage2.Controls.Add(this.txtvar4Value);
            this.tabPage2.Controls.Add(this.txtvar1Value);
            this.tabPage2.Controls.Add(this.txtvar5Value);
            this.tabPage2.Controls.Add(this.txtvar6);
            this.tabPage2.Controls.Add(this.txtvar6Value);
            this.tabPage2.Controls.Add(this.txtvar4);
            this.tabPage2.Controls.Add(this.txtvar3);
            this.tabPage2.Controls.Add(this.txtvar2);
            this.tabPage2.Controls.Add(this.txtvar5);
            this.tabPage2.Controls.Add(this.txtvar4Unit);
            this.tabPage2.Controls.Add(this.txtvar5Unit);
            this.tabPage2.Controls.Add(this.txtvar6Unit);
            this.tabPage2.Controls.Add(this.txtvar2Unit);
            this.tabPage2.Controls.Add(this.txtvar3Unit);
            this.tabPage2.Controls.Add(this.txtvar1Unit);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(454, 250);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Bảng 1";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // txtvar1
            // 
            this.txtvar1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar1.ForeColor = System.Drawing.Color.Black;
            this.txtvar1.Location = new System.Drawing.Point(34, 19);
            this.txtvar1.Name = "txtvar1";
            this.txtvar1.Size = new System.Drawing.Size(161, 19);
            this.txtvar1.TabIndex = 69;
            this.txtvar1.Text = "pH:";
            this.txtvar1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar1.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar1_Paint);
            // 
            // txtvar3Value
            // 
            this.txtvar3Value.BackColor = System.Drawing.Color.White;
            this.txtvar3Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar3Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar3Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar3Value.Location = new System.Drawing.Point(220, 99);
            this.txtvar3Value.Name = "txtvar3Value";
            this.txtvar3Value.ReadOnly = true;
            this.txtvar3Value.Size = new System.Drawing.Size(98, 19);
            this.txtvar3Value.TabIndex = 77;
            this.txtvar3Value.Text = "-";
            this.txtvar3Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar2Value
            // 
            this.txtvar2Value.BackColor = System.Drawing.Color.White;
            this.txtvar2Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar2Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar2Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar2Value.Location = new System.Drawing.Point(220, 62);
            this.txtvar2Value.Name = "txtvar2Value";
            this.txtvar2Value.ReadOnly = true;
            this.txtvar2Value.Size = new System.Drawing.Size(98, 19);
            this.txtvar2Value.TabIndex = 76;
            this.txtvar2Value.Text = "-";
            this.txtvar2Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar4Value
            // 
            this.txtvar4Value.BackColor = System.Drawing.Color.White;
            this.txtvar4Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar4Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar4Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar4Value.Location = new System.Drawing.Point(220, 135);
            this.txtvar4Value.Name = "txtvar4Value";
            this.txtvar4Value.ReadOnly = true;
            this.txtvar4Value.Size = new System.Drawing.Size(98, 19);
            this.txtvar4Value.TabIndex = 78;
            this.txtvar4Value.Text = "6.88";
            this.txtvar4Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar1Value
            // 
            this.txtvar1Value.BackColor = System.Drawing.Color.White;
            this.txtvar1Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar1Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar1Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar1Value.Location = new System.Drawing.Point(220, 19);
            this.txtvar1Value.Name = "txtvar1Value";
            this.txtvar1Value.Size = new System.Drawing.Size(98, 19);
            this.txtvar1Value.TabIndex = 75;
            this.txtvar1Value.Text = "-";
            this.txtvar1Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar5Value
            // 
            this.txtvar5Value.BackColor = System.Drawing.Color.White;
            this.txtvar5Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar5Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar5Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar5Value.Location = new System.Drawing.Point(220, 172);
            this.txtvar5Value.Name = "txtvar5Value";
            this.txtvar5Value.ReadOnly = true;
            this.txtvar5Value.Size = new System.Drawing.Size(98, 19);
            this.txtvar5Value.TabIndex = 79;
            this.txtvar5Value.Text = "9.29";
            this.txtvar5Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar6
            // 
            this.txtvar6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar6.ForeColor = System.Drawing.Color.Black;
            this.txtvar6.Location = new System.Drawing.Point(34, 204);
            this.txtvar6.Name = "txtvar6";
            this.txtvar6.Size = new System.Drawing.Size(161, 19);
            this.txtvar6.TabIndex = 74;
            this.txtvar6.Text = "EC:";
            this.txtvar6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar6.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar6_Paint);
            // 
            // txtvar6Value
            // 
            this.txtvar6Value.BackColor = System.Drawing.Color.White;
            this.txtvar6Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar6Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar6Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar6Value.Location = new System.Drawing.Point(220, 206);
            this.txtvar6Value.Name = "txtvar6Value";
            this.txtvar6Value.ReadOnly = true;
            this.txtvar6Value.Size = new System.Drawing.Size(98, 19);
            this.txtvar6Value.TabIndex = 80;
            this.txtvar6Value.Text = "117.242";
            this.txtvar6Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar4
            // 
            this.txtvar4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar4.ForeColor = System.Drawing.Color.Black;
            this.txtvar4.Location = new System.Drawing.Point(34, 134);
            this.txtvar4.Name = "txtvar4";
            this.txtvar4.Size = new System.Drawing.Size(161, 19);
            this.txtvar4.TabIndex = 72;
            this.txtvar4.Text = "DO:";
            this.txtvar4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar4.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar4_Paint);
            // 
            // txtvar3
            // 
            this.txtvar3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar3.ForeColor = System.Drawing.Color.Black;
            this.txtvar3.Location = new System.Drawing.Point(30, 98);
            this.txtvar3.Name = "txtvar3";
            this.txtvar3.Size = new System.Drawing.Size(165, 19);
            this.txtvar3.TabIndex = 71;
            this.txtvar3.Text = "Temp:";
            this.txtvar3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar3.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar3_Paint);
            // 
            // txtvar2
            // 
            this.txtvar2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar2.ForeColor = System.Drawing.Color.Black;
            this.txtvar2.Location = new System.Drawing.Point(30, 61);
            this.txtvar2.Name = "txtvar2";
            this.txtvar2.Size = new System.Drawing.Size(165, 19);
            this.txtvar2.TabIndex = 70;
            this.txtvar2.Text = "ORP:";
            this.txtvar2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar2.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar2_Paint);
            // 
            // txtvar5
            // 
            this.txtvar5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar5.ForeColor = System.Drawing.Color.Black;
            this.txtvar5.Location = new System.Drawing.Point(34, 171);
            this.txtvar5.Name = "txtvar5";
            this.txtvar5.Size = new System.Drawing.Size(161, 19);
            this.txtvar5.TabIndex = 73;
            this.txtvar5.Text = "TSS:";
            this.txtvar5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar5.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar5_Paint);
            // 
            // txtvar4Unit
            // 
            this.txtvar4Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar4Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar4Unit.Location = new System.Drawing.Point(342, 135);
            this.txtvar4Unit.Name = "txtvar4Unit";
            this.txtvar4Unit.Size = new System.Drawing.Size(91, 19);
            this.txtvar4Unit.TabIndex = 81;
            this.txtvar4Unit.Text = "mg/L";
            this.txtvar4Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar4Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar4Unit_Paint);
            // 
            // txtvar5Unit
            // 
            this.txtvar5Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar5Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar5Unit.Location = new System.Drawing.Point(343, 172);
            this.txtvar5Unit.Name = "txtvar5Unit";
            this.txtvar5Unit.Size = new System.Drawing.Size(90, 19);
            this.txtvar5Unit.TabIndex = 82;
            this.txtvar5Unit.Text = "mg/L";
            this.txtvar5Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar5Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar5Unit_Paint);
            // 
            // txtvar6Unit
            // 
            this.txtvar6Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar6Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar6Unit.Location = new System.Drawing.Point(342, 206);
            this.txtvar6Unit.Name = "txtvar6Unit";
            this.txtvar6Unit.Size = new System.Drawing.Size(91, 20);
            this.txtvar6Unit.TabIndex = 83;
            this.txtvar6Unit.Text = "uS/cm";
            this.txtvar6Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar6Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar6Unit_Paint);
            // 
            // txtvar2Unit
            // 
            this.txtvar2Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar2Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar2Unit.Location = new System.Drawing.Point(342, 62);
            this.txtvar2Unit.Name = "txtvar2Unit";
            this.txtvar2Unit.Size = new System.Drawing.Size(91, 19);
            this.txtvar2Unit.TabIndex = 84;
            this.txtvar2Unit.Text = "mV";
            this.txtvar2Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar2Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar2Unit_Paint);
            // 
            // txtvar3Unit
            // 
            this.txtvar3Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar3Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar3Unit.Location = new System.Drawing.Point(342, 99);
            this.txtvar3Unit.Name = "txtvar3Unit";
            this.txtvar3Unit.Size = new System.Drawing.Size(91, 19);
            this.txtvar3Unit.TabIndex = 85;
            this.txtvar3Unit.Text = "oC";
            this.txtvar3Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar3Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar3Unit_Paint);
            // 
            // txtvar1Unit
            // 
            this.txtvar1Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar1Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar1Unit.Location = new System.Drawing.Point(343, 19);
            this.txtvar1Unit.Name = "txtvar1Unit";
            this.txtvar1Unit.Size = new System.Drawing.Size(90, 19);
            this.txtvar1Unit.TabIndex = 86;
            this.txtvar1Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar1Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar1Unit_Paint);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtvar9Value);
            this.tabPage1.Controls.Add(this.txtvar8Value);
            this.tabPage1.Controls.Add(this.txtvar7Unit);
            this.tabPage1.Controls.Add(this.txtvar10Value);
            this.tabPage1.Controls.Add(this.txtvar9Unit);
            this.tabPage1.Controls.Add(this.txtvar7Value);
            this.tabPage1.Controls.Add(this.txtvar8Unit);
            this.tabPage1.Controls.Add(this.txtvar11Value);
            this.tabPage1.Controls.Add(this.txtvar12Unit);
            this.tabPage1.Controls.Add(this.txtvar12);
            this.tabPage1.Controls.Add(this.txtvar11Unit);
            this.tabPage1.Controls.Add(this.txtvar12Value);
            this.tabPage1.Controls.Add(this.txtvar10Unit);
            this.tabPage1.Controls.Add(this.txtvar10);
            this.tabPage1.Controls.Add(this.txtvar11);
            this.tabPage1.Controls.Add(this.txtvar9);
            this.tabPage1.Controls.Add(this.txtvar7);
            this.tabPage1.Controls.Add(this.txtvar8);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(454, 250);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Bảng 2";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtvar9Value
            // 
            this.txtvar9Value.BackColor = System.Drawing.Color.White;
            this.txtvar9Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar9Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar9Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar9Value.Location = new System.Drawing.Point(229, 93);
            this.txtvar9Value.Name = "txtvar9Value";
            this.txtvar9Value.ReadOnly = true;
            this.txtvar9Value.Size = new System.Drawing.Size(103, 19);
            this.txtvar9Value.TabIndex = 101;
            this.txtvar9Value.Text = "27.65";
            this.txtvar9Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar8Value
            // 
            this.txtvar8Value.BackColor = System.Drawing.Color.White;
            this.txtvar8Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar8Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar8Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar8Value.Location = new System.Drawing.Point(229, 58);
            this.txtvar8Value.Name = "txtvar8Value";
            this.txtvar8Value.ReadOnly = true;
            this.txtvar8Value.Size = new System.Drawing.Size(103, 19);
            this.txtvar8Value.TabIndex = 100;
            this.txtvar8Value.Text = "426.17";
            this.txtvar8Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar7Unit
            // 
            this.txtvar7Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar7Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar7Unit.Location = new System.Drawing.Point(348, 25);
            this.txtvar7Unit.Name = "txtvar7Unit";
            this.txtvar7Unit.Size = new System.Drawing.Size(83, 19);
            this.txtvar7Unit.TabIndex = 110;
            this.txtvar7Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar7Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar7Unit_Paint);
            // 
            // txtvar10Value
            // 
            this.txtvar10Value.BackColor = System.Drawing.Color.White;
            this.txtvar10Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar10Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar10Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar10Value.Location = new System.Drawing.Point(229, 129);
            this.txtvar10Value.Name = "txtvar10Value";
            this.txtvar10Value.ReadOnly = true;
            this.txtvar10Value.Size = new System.Drawing.Size(103, 19);
            this.txtvar10Value.TabIndex = 102;
            this.txtvar10Value.Text = "6.88";
            this.txtvar10Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar9Unit
            // 
            this.txtvar9Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar9Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar9Unit.Location = new System.Drawing.Point(348, 93);
            this.txtvar9Unit.Name = "txtvar9Unit";
            this.txtvar9Unit.Size = new System.Drawing.Size(83, 19);
            this.txtvar9Unit.TabIndex = 109;
            this.txtvar9Unit.Text = "oC";
            this.txtvar9Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar9Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar9Unit_Paint);
            // 
            // txtvar7Value
            // 
            this.txtvar7Value.BackColor = System.Drawing.Color.White;
            this.txtvar7Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar7Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar7Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar7Value.Location = new System.Drawing.Point(229, 25);
            this.txtvar7Value.Name = "txtvar7Value";
            this.txtvar7Value.Size = new System.Drawing.Size(103, 19);
            this.txtvar7Value.TabIndex = 99;
            this.txtvar7Value.Text = "7.20";
            this.txtvar7Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar8Unit
            // 
            this.txtvar8Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar8Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar8Unit.Location = new System.Drawing.Point(348, 58);
            this.txtvar8Unit.Name = "txtvar8Unit";
            this.txtvar8Unit.Size = new System.Drawing.Size(83, 19);
            this.txtvar8Unit.TabIndex = 108;
            this.txtvar8Unit.Text = "mV";
            this.txtvar8Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar8Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar8Unit_Paint);
            // 
            // txtvar11Value
            // 
            this.txtvar11Value.BackColor = System.Drawing.Color.White;
            this.txtvar11Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar11Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar11Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar11Value.Location = new System.Drawing.Point(229, 166);
            this.txtvar11Value.Name = "txtvar11Value";
            this.txtvar11Value.ReadOnly = true;
            this.txtvar11Value.Size = new System.Drawing.Size(103, 19);
            this.txtvar11Value.TabIndex = 103;
            this.txtvar11Value.Text = "9.29";
            this.txtvar11Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar12Unit
            // 
            this.txtvar12Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar12Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar12Unit.Location = new System.Drawing.Point(348, 199);
            this.txtvar12Unit.Name = "txtvar12Unit";
            this.txtvar12Unit.Size = new System.Drawing.Size(83, 20);
            this.txtvar12Unit.TabIndex = 107;
            this.txtvar12Unit.Text = "uS/cm";
            this.txtvar12Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar12Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar12Unit_Paint);
            // 
            // txtvar12
            // 
            this.txtvar12.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar12.ForeColor = System.Drawing.Color.Black;
            this.txtvar12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtvar12.Location = new System.Drawing.Point(34, 199);
            this.txtvar12.Name = "txtvar12";
            this.txtvar12.Size = new System.Drawing.Size(181, 19);
            this.txtvar12.TabIndex = 98;
            this.txtvar12.Text = "EC:";
            this.txtvar12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar12.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar12_Paint);
            // 
            // txtvar11Unit
            // 
            this.txtvar11Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar11Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar11Unit.Location = new System.Drawing.Point(348, 166);
            this.txtvar11Unit.Name = "txtvar11Unit";
            this.txtvar11Unit.Size = new System.Drawing.Size(83, 19);
            this.txtvar11Unit.TabIndex = 106;
            this.txtvar11Unit.Text = "mg/L";
            this.txtvar11Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar11Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar11Unit_Paint);
            // 
            // txtvar12Value
            // 
            this.txtvar12Value.BackColor = System.Drawing.Color.White;
            this.txtvar12Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar12Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar12Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar12Value.Location = new System.Drawing.Point(229, 199);
            this.txtvar12Value.Name = "txtvar12Value";
            this.txtvar12Value.ReadOnly = true;
            this.txtvar12Value.Size = new System.Drawing.Size(103, 19);
            this.txtvar12Value.TabIndex = 104;
            this.txtvar12Value.Text = "117.242";
            this.txtvar12Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar10Unit
            // 
            this.txtvar10Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar10Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar10Unit.Location = new System.Drawing.Point(348, 129);
            this.txtvar10Unit.Name = "txtvar10Unit";
            this.txtvar10Unit.Size = new System.Drawing.Size(83, 19);
            this.txtvar10Unit.TabIndex = 105;
            this.txtvar10Unit.Text = "mg/L";
            this.txtvar10Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar10Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar10Unit_Paint);
            // 
            // txtvar10
            // 
            this.txtvar10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar10.ForeColor = System.Drawing.Color.Black;
            this.txtvar10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtvar10.Location = new System.Drawing.Point(34, 129);
            this.txtvar10.Name = "txtvar10";
            this.txtvar10.Size = new System.Drawing.Size(181, 19);
            this.txtvar10.TabIndex = 96;
            this.txtvar10.Text = "DO:";
            this.txtvar10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar10.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar10_Paint);
            // 
            // txtvar11
            // 
            this.txtvar11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar11.ForeColor = System.Drawing.Color.Black;
            this.txtvar11.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtvar11.Location = new System.Drawing.Point(34, 166);
            this.txtvar11.Name = "txtvar11";
            this.txtvar11.Size = new System.Drawing.Size(181, 19);
            this.txtvar11.TabIndex = 97;
            this.txtvar11.Text = "TSS:";
            this.txtvar11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar11.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar11_Paint);
            // 
            // txtvar9
            // 
            this.txtvar9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar9.ForeColor = System.Drawing.Color.Black;
            this.txtvar9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtvar9.Location = new System.Drawing.Point(30, 93);
            this.txtvar9.Name = "txtvar9";
            this.txtvar9.Size = new System.Drawing.Size(185, 19);
            this.txtvar9.TabIndex = 95;
            this.txtvar9.Text = "Temp:";
            this.txtvar9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar9.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar9_Paint);
            // 
            // txtvar7
            // 
            this.txtvar7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar7.ForeColor = System.Drawing.Color.Black;
            this.txtvar7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtvar7.Location = new System.Drawing.Point(34, 25);
            this.txtvar7.Name = "txtvar7";
            this.txtvar7.Size = new System.Drawing.Size(181, 19);
            this.txtvar7.TabIndex = 93;
            this.txtvar7.Text = "pH:";
            this.txtvar7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar7.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar7_Paint);
            // 
            // txtvar8
            // 
            this.txtvar8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar8.ForeColor = System.Drawing.Color.Black;
            this.txtvar8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtvar8.Location = new System.Drawing.Point(30, 58);
            this.txtvar8.Name = "txtvar8";
            this.txtvar8.Size = new System.Drawing.Size(185, 19);
            this.txtvar8.TabIndex = 94;
            this.txtvar8.Text = "ORP:";
            this.txtvar8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar8.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar8_Paint);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtvar13Unit);
            this.tabPage3.Controls.Add(this.txtvar15Unit);
            this.tabPage3.Controls.Add(this.txtvar14Unit);
            this.tabPage3.Controls.Add(this.txtvar18Unit);
            this.tabPage3.Controls.Add(this.txtvar17Unit);
            this.tabPage3.Controls.Add(this.txtvar16Unit);
            this.tabPage3.Controls.Add(this.txtvar17);
            this.tabPage3.Controls.Add(this.txtvar13);
            this.tabPage3.Controls.Add(this.txtvar14);
            this.tabPage3.Controls.Add(this.txtvar15);
            this.tabPage3.Controls.Add(this.txtvar16);
            this.tabPage3.Controls.Add(this.txtvar18Value);
            this.tabPage3.Controls.Add(this.txtvar18);
            this.tabPage3.Controls.Add(this.txtvar17Value);
            this.tabPage3.Controls.Add(this.txtvar13Value);
            this.tabPage3.Controls.Add(this.txtvar16Value);
            this.tabPage3.Controls.Add(this.txtvar14Value);
            this.tabPage3.Controls.Add(this.txtvar15Value);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(454, 250);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Bảng 3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtvar13Unit
            // 
            this.txtvar13Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar13Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar13Unit.Location = new System.Drawing.Point(359, 23);
            this.txtvar13Unit.Name = "txtvar13Unit";
            this.txtvar13Unit.Size = new System.Drawing.Size(75, 19);
            this.txtvar13Unit.TabIndex = 128;
            this.txtvar13Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar13Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar13Unit_Paint);
            // 
            // txtvar15Unit
            // 
            this.txtvar15Unit.AutoSize = true;
            this.txtvar15Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar15Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar15Unit.Location = new System.Drawing.Point(359, 96);
            this.txtvar15Unit.Name = "txtvar15Unit";
            this.txtvar15Unit.Size = new System.Drawing.Size(28, 19);
            this.txtvar15Unit.TabIndex = 127;
            this.txtvar15Unit.Text = "oC";
            this.txtvar15Unit.Visible = false;
            // 
            // txtvar14Unit
            // 
            this.txtvar14Unit.AutoSize = true;
            this.txtvar14Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar14Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar14Unit.Location = new System.Drawing.Point(359, 62);
            this.txtvar14Unit.Name = "txtvar14Unit";
            this.txtvar14Unit.Size = new System.Drawing.Size(32, 19);
            this.txtvar14Unit.TabIndex = 126;
            this.txtvar14Unit.Text = "mV";
            this.txtvar14Unit.Visible = false;
            // 
            // txtvar18Unit
            // 
            this.txtvar18Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar18Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar18Unit.Location = new System.Drawing.Point(359, 200);
            this.txtvar18Unit.Name = "txtvar18Unit";
            this.txtvar18Unit.Size = new System.Drawing.Size(75, 20);
            this.txtvar18Unit.TabIndex = 125;
            this.txtvar18Unit.Text = "uS/cm";
            this.txtvar18Unit.Visible = false;
            // 
            // txtvar17Unit
            // 
            this.txtvar17Unit.AutoSize = true;
            this.txtvar17Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar17Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar17Unit.Location = new System.Drawing.Point(359, 164);
            this.txtvar17Unit.Name = "txtvar17Unit";
            this.txtvar17Unit.Size = new System.Drawing.Size(43, 19);
            this.txtvar17Unit.TabIndex = 124;
            this.txtvar17Unit.Text = "mg/L";
            this.txtvar17Unit.Visible = false;
            // 
            // txtvar16Unit
            // 
            this.txtvar16Unit.AutoSize = true;
            this.txtvar16Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar16Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar16Unit.Location = new System.Drawing.Point(359, 131);
            this.txtvar16Unit.Name = "txtvar16Unit";
            this.txtvar16Unit.Size = new System.Drawing.Size(43, 19);
            this.txtvar16Unit.TabIndex = 123;
            this.txtvar16Unit.Text = "mg/L";
            this.txtvar16Unit.Visible = false;
            // 
            // txtvar17
            // 
            this.txtvar17.AutoSize = true;
            this.txtvar17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar17.ForeColor = System.Drawing.Color.Black;
            this.txtvar17.Location = new System.Drawing.Point(23, 164);
            this.txtvar17.Name = "txtvar17";
            this.txtvar17.Size = new System.Drawing.Size(42, 19);
            this.txtvar17.TabIndex = 115;
            this.txtvar17.Text = "TSS:";
            this.txtvar17.Visible = false;
            // 
            // txtvar13
            // 
            this.txtvar13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar13.ForeColor = System.Drawing.Color.Black;
            this.txtvar13.Location = new System.Drawing.Point(23, 23);
            this.txtvar13.Name = "txtvar13";
            this.txtvar13.Size = new System.Drawing.Size(202, 19);
            this.txtvar13.TabIndex = 111;
            this.txtvar13.Text = "pH:";
            this.txtvar13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar13.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar13_Paint);
            // 
            // txtvar14
            // 
            this.txtvar14.AutoSize = true;
            this.txtvar14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar14.ForeColor = System.Drawing.Color.Black;
            this.txtvar14.Location = new System.Drawing.Point(18, 62);
            this.txtvar14.Name = "txtvar14";
            this.txtvar14.Size = new System.Drawing.Size(47, 19);
            this.txtvar14.TabIndex = 112;
            this.txtvar14.Text = "ORP:";
            this.txtvar14.Visible = false;
            // 
            // txtvar15
            // 
            this.txtvar15.AutoSize = true;
            this.txtvar15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar15.ForeColor = System.Drawing.Color.Black;
            this.txtvar15.Location = new System.Drawing.Point(14, 96);
            this.txtvar15.Name = "txtvar15";
            this.txtvar15.Size = new System.Drawing.Size(51, 19);
            this.txtvar15.TabIndex = 113;
            this.txtvar15.Text = "Temp:";
            this.txtvar15.Visible = false;
            // 
            // txtvar16
            // 
            this.txtvar16.AutoSize = true;
            this.txtvar16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar16.ForeColor = System.Drawing.Color.Black;
            this.txtvar16.Location = new System.Drawing.Point(27, 131);
            this.txtvar16.Name = "txtvar16";
            this.txtvar16.Size = new System.Drawing.Size(38, 19);
            this.txtvar16.TabIndex = 114;
            this.txtvar16.Text = "DO:";
            this.txtvar16.Visible = false;
            // 
            // txtvar18Value
            // 
            this.txtvar18Value.BackColor = System.Drawing.Color.White;
            this.txtvar18Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar18Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar18Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar18Value.Location = new System.Drawing.Point(242, 196);
            this.txtvar18Value.Name = "txtvar18Value";
            this.txtvar18Value.ReadOnly = true;
            this.txtvar18Value.Size = new System.Drawing.Size(96, 19);
            this.txtvar18Value.TabIndex = 122;
            this.txtvar18Value.Text = "117.242";
            this.txtvar18Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar18Value.Visible = false;
            // 
            // txtvar18
            // 
            this.txtvar18.AutoSize = true;
            this.txtvar18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar18.ForeColor = System.Drawing.Color.Black;
            this.txtvar18.Location = new System.Drawing.Point(30, 200);
            this.txtvar18.Name = "txtvar18";
            this.txtvar18.Size = new System.Drawing.Size(35, 19);
            this.txtvar18.TabIndex = 116;
            this.txtvar18.Text = "EC:";
            this.txtvar18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar18.Visible = false;
            // 
            // txtvar17Value
            // 
            this.txtvar17Value.BackColor = System.Drawing.Color.White;
            this.txtvar17Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar17Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar17Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar17Value.Location = new System.Drawing.Point(242, 160);
            this.txtvar17Value.Name = "txtvar17Value";
            this.txtvar17Value.ReadOnly = true;
            this.txtvar17Value.Size = new System.Drawing.Size(96, 19);
            this.txtvar17Value.TabIndex = 121;
            this.txtvar17Value.Text = "9.29";
            this.txtvar17Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar17Value.Visible = false;
            // 
            // txtvar13Value
            // 
            this.txtvar13Value.BackColor = System.Drawing.Color.White;
            this.txtvar13Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar13Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar13Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar13Value.Location = new System.Drawing.Point(242, 23);
            this.txtvar13Value.Name = "txtvar13Value";
            this.txtvar13Value.Size = new System.Drawing.Size(96, 19);
            this.txtvar13Value.TabIndex = 117;
            this.txtvar13Value.Text = "7.20";
            this.txtvar13Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar16Value
            // 
            this.txtvar16Value.BackColor = System.Drawing.Color.White;
            this.txtvar16Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar16Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar16Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar16Value.Location = new System.Drawing.Point(242, 127);
            this.txtvar16Value.Name = "txtvar16Value";
            this.txtvar16Value.ReadOnly = true;
            this.txtvar16Value.Size = new System.Drawing.Size(96, 19);
            this.txtvar16Value.TabIndex = 120;
            this.txtvar16Value.Text = "6.88";
            this.txtvar16Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar16Value.Visible = false;
            // 
            // txtvar14Value
            // 
            this.txtvar14Value.BackColor = System.Drawing.Color.White;
            this.txtvar14Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar14Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar14Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar14Value.Location = new System.Drawing.Point(242, 58);
            this.txtvar14Value.Name = "txtvar14Value";
            this.txtvar14Value.ReadOnly = true;
            this.txtvar14Value.Size = new System.Drawing.Size(96, 19);
            this.txtvar14Value.TabIndex = 118;
            this.txtvar14Value.Text = "426.17";
            this.txtvar14Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar14Value.Visible = false;
            // 
            // txtvar15Value
            // 
            this.txtvar15Value.BackColor = System.Drawing.Color.White;
            this.txtvar15Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar15Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar15Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar15Value.Location = new System.Drawing.Point(242, 92);
            this.txtvar15Value.Name = "txtvar15Value";
            this.txtvar15Value.ReadOnly = true;
            this.txtvar15Value.Size = new System.Drawing.Size(96, 19);
            this.txtvar15Value.TabIndex = 119;
            this.txtvar15Value.Text = "27.65";
            this.txtvar15Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar15Value.Visible = false;
            // 
            // frm1HourMPS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.BackgroundImage = global::DataLogger.Properties.Resources._5_min_Data;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(654, 330);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm1HourMPS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm1HourMPS_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblHeaderTitle;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.Label txtvar1;
        public System.Windows.Forms.TextBox txtvar3Value;
        public System.Windows.Forms.TextBox txtvar2Value;
        public System.Windows.Forms.TextBox txtvar4Value;
        public System.Windows.Forms.TextBox txtvar1Value;
        public System.Windows.Forms.TextBox txtvar5Value;
        public System.Windows.Forms.Label txtvar6;
        public System.Windows.Forms.TextBox txtvar6Value;
        public System.Windows.Forms.Label txtvar4;
        public System.Windows.Forms.Label txtvar3;
        public System.Windows.Forms.Label txtvar2;
        public System.Windows.Forms.Label txtvar5;
        public System.Windows.Forms.Label txtvar4Unit;
        public System.Windows.Forms.Label txtvar5Unit;
        public System.Windows.Forms.Label txtvar6Unit;
        public System.Windows.Forms.Label txtvar2Unit;
        public System.Windows.Forms.Label txtvar3Unit;
        public System.Windows.Forms.Label txtvar1Unit;
        private System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.TextBox txtvar9Value;
        public System.Windows.Forms.TextBox txtvar8Value;
        public System.Windows.Forms.Label txtvar7Unit;
        public System.Windows.Forms.TextBox txtvar10Value;
        public System.Windows.Forms.Label txtvar9Unit;
        public System.Windows.Forms.TextBox txtvar7Value;
        public System.Windows.Forms.Label txtvar8Unit;
        public System.Windows.Forms.TextBox txtvar11Value;
        public System.Windows.Forms.Label txtvar12Unit;
        public System.Windows.Forms.Label txtvar12;
        public System.Windows.Forms.Label txtvar11Unit;
        public System.Windows.Forms.TextBox txtvar12Value;
        public System.Windows.Forms.Label txtvar10Unit;
        public System.Windows.Forms.Label txtvar10;
        public System.Windows.Forms.Label txtvar11;
        public System.Windows.Forms.Label txtvar9;
        public System.Windows.Forms.Label txtvar7;
        public System.Windows.Forms.Label txtvar8;
        private System.Windows.Forms.TabPage tabPage3;
        public System.Windows.Forms.Label txtvar13Unit;
        public System.Windows.Forms.Label txtvar15Unit;
        public System.Windows.Forms.Label txtvar14Unit;
        public System.Windows.Forms.Label txtvar18Unit;
        public System.Windows.Forms.Label txtvar17Unit;
        public System.Windows.Forms.Label txtvar16Unit;
        public System.Windows.Forms.Label txtvar17;
        public System.Windows.Forms.Label txtvar13;
        public System.Windows.Forms.Label txtvar14;
        public System.Windows.Forms.Label txtvar15;
        public System.Windows.Forms.Label txtvar16;
        public System.Windows.Forms.TextBox txtvar18Value;
        public System.Windows.Forms.Label txtvar18;
        public System.Windows.Forms.TextBox txtvar17Value;
        public System.Windows.Forms.TextBox txtvar13Value;
        public System.Windows.Forms.TextBox txtvar16Value;
        public System.Windows.Forms.TextBox txtvar14Value;
        public System.Windows.Forms.TextBox txtvar15Value;
    }
}